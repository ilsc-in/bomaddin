# Installation Instructions

1. Uninstall any previous version of BOMAddIn.
2. Download BOMAddIn installer zip file using this link. [BOMAddIn.zip](https://bitbucket.org/ilsc-in/bomaddin/src/master/BOMAddIn/publish/BOMAddIn.zip). Click on View Raw link to start download.
![ViewRaw.png](https://bitbucket.org/repo/8Xenk79/images/225930487-ViewRaw.png)
3. Install [Visual Studio Tools for Office runtime](https://www.microsoft.com/download/details.aspx?id=56961) if not previously installed.
4. Install certificate as Trusted CA Certificate. Follow the steps shown in the figures below.
![ImportCertificate_1.png](https://bitbucket.org/repo/8Xenk79/images/1122198177-ImportCertificate_1.png)
![ImportCertificate_2.png](https://bitbucket.org/repo/8Xenk79/images/1472228049-ImportCertificate_2.png)
![ImportCertificate_3.png](https://bitbucket.org/repo/8Xenk79/images/3697710227-ImportCertificate_3.png)
4. Unzip BOMAddIn.zip and install by running setup.exe found in the zip file.
5. Place the bom_config.xml file found in the zip file at a centralised network location.
6. Add an Environment Variable named `BOM_CFG_FILE` to locate the config file. Follow the steps shown in the image below.
![SetEnvVariable.png](https://bitbucket.org/repo/8Xenk79/images/3456457539-SetEnvVariable.png)