﻿using System;

namespace Insight.Fusion.BOMAddIn
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            Ribbon?.ToggleButtons(true);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Ribbon?.ToggleButtons(true);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.CmbEnv = new System.Windows.Forms.ComboBox();
            this.lblPass = new System.Windows.Forms.Label();
            this.TxtPass = new System.Windows.Forms.TextBox();
            this.lblEnv = new System.Windows.Forms.Label();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.ChkSavePassword = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtUser
            // 
            this.TxtUser.Location = new System.Drawing.Point(249, 17);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.Size = new System.Drawing.Size(181, 20);
            this.TxtUser.TabIndex = 0;
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(189, 20);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(55, 13);
            this.lblUser.TabIndex = 1;
            this.lblUser.Text = "Username";
            // 
            // BtnLogin
            // 
            this.BtnLogin.Location = new System.Drawing.Point(249, 161);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(75, 23);
            this.BtnLogin.TabIndex = 4;
            this.BtnLogin.Text = "Login";
            this.BtnLogin.UseVisualStyleBackColor = true;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // pbLogo
            // 
            this.pbLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbLogo.Image")));
            this.pbLogo.InitialImage = null;
            this.pbLogo.Location = new System.Drawing.Point(13, 17);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(159, 167);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbLogo.TabIndex = 3;
            this.pbLogo.TabStop = false;
            // 
            // CmbEnv
            // 
            this.CmbEnv.FormattingEnabled = true;
            this.CmbEnv.Location = new System.Drawing.Point(249, 95);
            this.CmbEnv.Name = "CmbEnv";
            this.CmbEnv.Size = new System.Drawing.Size(181, 21);
            this.CmbEnv.TabIndex = 3;
            this.CmbEnv.SelectedIndexChanged += new System.EventHandler(this.CmbEnv_SelectedIndexChanged);
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Location = new System.Drawing.Point(189, 59);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(53, 13);
            this.lblPass.TabIndex = 6;
            this.lblPass.Text = "Password";
            // 
            // TxtPass
            // 
            this.TxtPass.Location = new System.Drawing.Point(249, 56);
            this.TxtPass.Name = "TxtPass";
            this.TxtPass.PasswordChar = '*';
            this.TxtPass.Size = new System.Drawing.Size(181, 20);
            this.TxtPass.TabIndex = 2;
            // 
            // lblEnv
            // 
            this.lblEnv.AutoSize = true;
            this.lblEnv.Location = new System.Drawing.Point(178, 98);
            this.lblEnv.Name = "lblEnv";
            this.lblEnv.Size = new System.Drawing.Size(66, 13);
            this.lblEnv.TabIndex = 7;
            this.lblEnv.Text = "Environment";
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(355, 161);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 5;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // ChkSavePassword
            // 
            this.ChkSavePassword.AutoSize = true;
            this.ChkSavePassword.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ChkSavePassword.Location = new System.Drawing.Point(249, 132);
            this.ChkSavePassword.Name = "ChkSavePassword";
            this.ChkSavePassword.Size = new System.Drawing.Size(100, 17);
            this.ChkSavePassword.TabIndex = 8;
            this.ChkSavePassword.Text = "Save Password";
            this.ChkSavePassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ChkSavePassword.UseVisualStyleBackColor = true;
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 204);
            this.ControlBox = false;
            this.Controls.Add(this.ChkSavePassword);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.lblEnv);
            this.Controls.Add(this.lblPass);
            this.Controls.Add(this.TxtPass);
            this.Controls.Add(this.CmbEnv);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.BtnLogin);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.TxtUser);
            this.Name = "FormLogin";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.FormLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.ComboBox CmbEnv;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.TextBox TxtPass;
        private System.Windows.Forms.Label lblEnv;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.CheckBox ChkSavePassword;
    }
}