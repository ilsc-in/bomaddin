﻿namespace Insight.Fusion.BOMAddIn
{
    partial class FormPerformActions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CirProgressBar = new CircularProgressBar.CircularProgressBar();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.dgLog = new System.Windows.Forms.DataGridView();
            this.Status = new System.Windows.Forms.DataGridViewImageColumn();
            this.Message = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).BeginInit();
            this.SuspendLayout();
            // 
            // circularProgressBar1
            // 
            this.CirProgressBar.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.CirProgressBar.AnimationSpeed = 500;
            this.CirProgressBar.BackColor = System.Drawing.Color.Transparent;
            this.CirProgressBar.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CirProgressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CirProgressBar.InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.CirProgressBar.InnerMargin = 0;
            this.CirProgressBar.InnerWidth = 0;
            this.CirProgressBar.Location = new System.Drawing.Point(357, 241);
            this.CirProgressBar.MarqueeAnimationSpeed = 2000;
            this.CirProgressBar.Name = "circularProgressBar1";
            this.CirProgressBar.OuterColor = System.Drawing.Color.Gray;
            this.CirProgressBar.OuterMargin = -4;
            this.CirProgressBar.OuterWidth = 2;
            this.CirProgressBar.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.CirProgressBar.ProgressWidth = 6;
            this.CirProgressBar.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CirProgressBar.Size = new System.Drawing.Size(92, 88);
            this.CirProgressBar.StartAngle = 270;
            this.CirProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.CirProgressBar.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.CirProgressBar.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.CirProgressBar.SubscriptText = "";
            this.CirProgressBar.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.CirProgressBar.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.CirProgressBar.SuperscriptText = "";
            this.CirProgressBar.TabIndex = 3;
            this.CirProgressBar.Text = "Please Wait";
            this.CirProgressBar.TextMargin = new System.Windows.Forms.Padding(0);
            this.CirProgressBar.Value = 90;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(26, 353);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(749, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 2;
            // 
            // dgLog
            // 
            this.dgLog.AllowUserToAddRows = false;
            this.dgLog.AllowUserToDeleteRows = false;
            this.dgLog.AllowUserToResizeColumns = false;
            this.dgLog.AllowUserToResizeRows = false;
            this.dgLog.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgLog.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgLog.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLog.ColumnHeadersVisible = false;
            this.dgLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Status,
            this.Message});
            this.dgLog.Location = new System.Drawing.Point(26, 33);
            this.dgLog.Name = "dgLog";
            this.dgLog.RowHeadersVisible = false;
            this.dgLog.ShowCellErrors = false;
            this.dgLog.ShowCellToolTips = false;
            this.dgLog.ShowEditingIcon = false;
            this.dgLog.ShowRowErrors = false;
            this.dgLog.Size = new System.Drawing.Size(749, 188);
            this.dgLog.TabIndex = 4;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // Message
            // 
            this.Message.HeaderText = "Message";
            this.Message.Name = "Message";
            this.Message.ReadOnly = true;
            this.Message.Width = 400;
            // 
            // BtnClose
            // 
            this.BtnClose.Location = new System.Drawing.Point(319, 396);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(154, 23);
            this.BtnClose.TabIndex = 5;
            this.BtnClose.Text = "Close";
            this.BtnClose.UseVisualStyleBackColor = true;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // FormPerformActions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 436);
            this.Controls.Add(this.BtnClose);
            this.Controls.Add(this.dgLog);
            this.Controls.Add(this.CirProgressBar);
            this.Controls.Add(this.progressBar);
            this.Name = "FormPerformActions";
            this.Text = "FormPerformActions";
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CircularProgressBar.CircularProgressBar CirProgressBar;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.DataGridView dgLog;
        private System.Windows.Forms.DataGridViewImageColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Message;
        private System.Windows.Forms.Button BtnClose;
    }
}