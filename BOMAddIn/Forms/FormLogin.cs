﻿using System;
using System.Windows.Forms;
using Insight.Fusion.Utils;

namespace Insight.Fusion.BOMAddIn
{
    public partial class FormLogin : Form
    {
        private FusionRibbon Ribbon;

        public FormLogin(FusionRibbon ribbon)
        {
            InitializeComponent();
            this.Ribbon = ribbon;
            ribbon.ToggleButtons(true);

            if (Ribbon.EnvList != null && Ribbon.EnvList.Count > 0)
            {
                this.CmbEnv.Items.AddRange(Ribbon.EnvList.ToArray());
                this.CmbEnv.SelectedIndex = Ribbon.EnvList.IndexOf(Ribbon.Env);
            }
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            Ribbon.Authenticated = false;

            FusionEnv env = CmbEnv.SelectedItem as FusionEnv;
            if (String.IsNullOrWhiteSpace(TxtUser.Text) || String.IsNullOrWhiteSpace(TxtPass.Text) || env == null)
            {
                MessageBox.Show("Username, Password or Environment can't be left blank.");
            }

            env.SetCredentials(TxtUser.Text, TxtPass.Text);
            Ribbon.Env = env;
            if (ChkSavePassword.Checked)
            {
                FusionEnv.SaveToFile(Ribbon.EnvList);
            }

            Ribbon.Client.Env = env;

            if (Ribbon.Client.Authenticate())
            {
                Ribbon.Authenticated = true;
                this.Hide();
                this.Close();
            }
            else MessageBox.Show("Login into Fusion failed.");
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }

        private void CmbEnv_SelectedIndexChanged(object sender, EventArgs e)
        {
            var env = this.CmbEnv.SelectedItem as FusionEnv;
            Ribbon.Env = env;
            if (env?.Credentials != null)
            {
                this.TxtUser.Text = env.GetUserName();
                this.TxtPass.Text = env.GetPassword();
            }
            else
            {
                this.TxtUser.Text = "";
                this.TxtPass.Text = "";
            }
        }
    }
}
