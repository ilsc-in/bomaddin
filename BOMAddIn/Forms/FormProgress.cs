﻿using Insight.Fusion.Utils;
using System;
using System.Windows.Forms;

namespace Insight.Fusion.BOMAddIn
{
    public partial class FormProgress : Form, IProgressTracker
    {
        private FusionRibbon Ribbon;

        public FormProgress(FusionRibbon ribbon)
        {
            InitializeComponent();
            this.Ribbon = ribbon;
        }

        private void FormProgress_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }

        public void OnCompletion()
        {
            Ribbon.ToggleButtons(true);
            if (this.InvokeRequired)
            {
                this.Invoke((Action)delegate { Hide(); });
            }
            else
            {
                Hide();
            }
        }

        public void Init(string description, int total)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((Action)delegate
            {
                if (!String.IsNullOrWhiteSpace(description))
                    Text = description;
            });
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(description))
                    Text = description;
            }

            if (progressBar.InvokeRequired)
            {
                progressBar.Invoke((Action)delegate
                {
                    progressBar.Style = ProgressBarStyle.Blocks;
                    progressBar.Minimum = 0;
                    progressBar.Maximum = total;

                    progressBar.Value = 0;
                });
            }
            else
            {
                progressBar.Style = ProgressBarStyle.Blocks;
                progressBar.Minimum = 0;
                progressBar.Maximum = total;

                progressBar.Value = 0;
            }
            

            Ribbon.ToggleButtons(false);

            if (this.InvokeRequired)
            {
                this.Invoke((Action)delegate
            {
                this.Show();
            });
            }
            else
            {
                this.Show();
            }
        }

        public void SetProgress(int current, int total)
        {
            if (progressBar.InvokeRequired)
            {
                progressBar.Invoke((Action)delegate
            {
                progressBar.Value = current < total ? current : total;
            });
            }
            else
            {
                progressBar.Value = current < total ? current : total;
            }
        }
    }
}
