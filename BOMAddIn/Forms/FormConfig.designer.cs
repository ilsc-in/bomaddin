﻿namespace Insight.Fusion.BOMAddIn
{
    partial class FormConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            Ribbon?.ToggleButtons(true);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgEnv = new System.Windows.Forms.DataGridView();
            this.Environment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.URL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Credentials = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtName = new System.Windows.Forms.TextBox();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.TxtURL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.TabCtl = new System.Windows.Forms.TabControl();
            this.EnvDetails = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtProduct = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnDelMap = new System.Windows.Forms.Button();
            this.BtnAddMap = new System.Windows.Forms.Button();
            this.TxtDefTemplate = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtIMOrg = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtDefClass = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtOrg = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtMatType = new System.Windows.Forms.TextBox();
            this.dgMap = new System.Windows.Forms.DataGridView();
            this.OrgCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemTemplate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TxtTemplate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtPass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtUser = new System.Windows.Forms.TextBox();
            this.ProcessTypes = new System.Windows.Forms.TabPage();
            this.BtnDelPType = new System.Windows.Forms.Button();
            this.dgDetails = new System.Windows.Forms.DataGridView();
            this.dgProcessTypes = new System.Windows.Forms.DataGridView();
            this.BtnDown = new System.Windows.Forms.Button();
            this.BtnUp = new System.Windows.Forms.Button();
            this.BtnDelPDet = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtEnd = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtStart = new System.Windows.Forms.TextBox();
            this.TxtPType = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.BtnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgEnv)).BeginInit();
            this.TabCtl.SuspendLayout();
            this.EnvDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMap)).BeginInit();
            this.ProcessTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgProcessTypes)).BeginInit();
            this.SuspendLayout();
            // 
            // dgEnv
            // 
            this.dgEnv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEnv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Environment,
            this.URL,
            this.Credentials});
            this.dgEnv.Location = new System.Drawing.Point(33, 54);
            this.dgEnv.Name = "dgEnv";
            this.dgEnv.Size = new System.Drawing.Size(1081, 132);
            this.dgEnv.TabIndex = 0;
            // 
            // Environment
            // 
            this.Environment.HeaderText = "Environment";
            this.Environment.Name = "Environment";
            this.Environment.ReadOnly = true;
            this.Environment.Width = 180;
            // 
            // URL
            // 
            this.URL.HeaderText = "URL";
            this.URL.Name = "URL";
            this.URL.ReadOnly = true;
            this.URL.Width = 500;
            // 
            // Credentials
            // 
            this.Credentials.HeaderText = "Credentials";
            this.Credentials.Name = "Credentials";
            this.Credentials.ReadOnly = true;
            this.Credentials.Width = 350;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "*Name";
            // 
            // TxtName
            // 
            this.TxtName.Location = new System.Drawing.Point(32, 28);
            this.TxtName.Name = "TxtName";
            this.TxtName.Size = new System.Drawing.Size(120, 20);
            this.TxtName.TabIndex = 2;
            // 
            // BtnAdd
            // 
            this.BtnAdd.Location = new System.Drawing.Point(1025, 26);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(75, 23);
            this.BtnAdd.TabIndex = 3;
            this.BtnAdd.Text = "Add";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // TxtURL
            // 
            this.TxtURL.Location = new System.Drawing.Point(158, 28);
            this.TxtURL.Name = "TxtURL";
            this.TxtURL.Size = new System.Drawing.Size(383, 20);
            this.TxtURL.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(155, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "*URL";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(1025, 192);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(75, 23);
            this.BtnDelete.TabIndex = 6;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Enabled = false;
            this.BtnSave.Location = new System.Drawing.Point(846, 549);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(131, 23);
            this.BtnSave.TabIndex = 7;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // TabCtl
            // 
            this.TabCtl.Controls.Add(this.EnvDetails);
            this.TabCtl.Controls.Add(this.ProcessTypes);
            this.TabCtl.Location = new System.Drawing.Point(12, 22);
            this.TabCtl.Name = "TabCtl";
            this.TabCtl.SelectedIndex = 0;
            this.TabCtl.Size = new System.Drawing.Size(1153, 503);
            this.TabCtl.TabIndex = 8;
            // 
            // EnvDetails
            // 
            this.EnvDetails.Controls.Add(this.label16);
            this.EnvDetails.Controls.Add(this.TxtProduct);
            this.EnvDetails.Controls.Add(this.label11);
            this.EnvDetails.Controls.Add(this.btnDelMap);
            this.EnvDetails.Controls.Add(this.BtnAddMap);
            this.EnvDetails.Controls.Add(this.TxtDefTemplate);
            this.EnvDetails.Controls.Add(this.label10);
            this.EnvDetails.Controls.Add(this.TxtIMOrg);
            this.EnvDetails.Controls.Add(this.label9);
            this.EnvDetails.Controls.Add(this.TxtDefClass);
            this.EnvDetails.Controls.Add(this.label8);
            this.EnvDetails.Controls.Add(this.label7);
            this.EnvDetails.Controls.Add(this.TxtOrg);
            this.EnvDetails.Controls.Add(this.label5);
            this.EnvDetails.Controls.Add(this.TxtMatType);
            this.EnvDetails.Controls.Add(this.dgMap);
            this.EnvDetails.Controls.Add(this.TxtTemplate);
            this.EnvDetails.Controls.Add(this.label6);
            this.EnvDetails.Controls.Add(this.label4);
            this.EnvDetails.Controls.Add(this.TxtPass);
            this.EnvDetails.Controls.Add(this.label3);
            this.EnvDetails.Controls.Add(this.TxtUser);
            this.EnvDetails.Controls.Add(this.BtnAdd);
            this.EnvDetails.Controls.Add(this.label1);
            this.EnvDetails.Controls.Add(this.BtnDelete);
            this.EnvDetails.Controls.Add(this.TxtName);
            this.EnvDetails.Controls.Add(this.dgEnv);
            this.EnvDetails.Controls.Add(this.TxtURL);
            this.EnvDetails.Controls.Add(this.label2);
            this.EnvDetails.Location = new System.Drawing.Point(4, 22);
            this.EnvDetails.Name = "EnvDetails";
            this.EnvDetails.Padding = new System.Windows.Forms.Padding(3);
            this.EnvDetails.Size = new System.Drawing.Size(1145, 477);
            this.EnvDetails.TabIndex = 0;
            this.EnvDetails.Text = "Environment Details";
            this.EnvDetails.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(120, 247);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 27;
            this.label16.Text = "*Product";
            // 
            // TxtProduct
            // 
            this.TxtProduct.Location = new System.Drawing.Point(123, 266);
            this.TxtProduct.Name = "TxtProduct";
            this.TxtProduct.Size = new System.Drawing.Size(148, 20);
            this.TxtProduct.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Location = new System.Drawing.Point(-6, 226);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(1152, 2);
            this.label11.TabIndex = 26;
            // 
            // btnDelMap
            // 
            this.btnDelMap.Location = new System.Drawing.Point(673, 444);
            this.btnDelMap.Name = "btnDelMap";
            this.btnDelMap.Size = new System.Drawing.Size(75, 23);
            this.btnDelMap.TabIndex = 25;
            this.btnDelMap.Text = "Delete";
            this.btnDelMap.UseVisualStyleBackColor = true;
            this.btnDelMap.Click += new System.EventHandler(this.BtnDelMap_Click);
            // 
            // BtnAddMap
            // 
            this.BtnAddMap.Location = new System.Drawing.Point(673, 264);
            this.BtnAddMap.Name = "BtnAddMap";
            this.BtnAddMap.Size = new System.Drawing.Size(75, 23);
            this.BtnAddMap.TabIndex = 24;
            this.BtnAddMap.Text = "Add";
            this.BtnAddMap.UseVisualStyleBackColor = true;
            this.BtnAddMap.Click += new System.EventHandler(this.BtnAddMap_Click);
            // 
            // TxtDefTemplate
            // 
            this.TxtDefTemplate.Location = new System.Drawing.Point(799, 373);
            this.TxtDefTemplate.Name = "TxtDefTemplate";
            this.TxtDefTemplate.Size = new System.Drawing.Size(301, 20);
            this.TxtDefTemplate.TabIndex = 23;
            this.TxtDefTemplate.TextChanged += new System.EventHandler(this.TxtDefClass_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(796, 357);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "*Default Item Template";
            // 
            // TxtIMOrg
            // 
            this.TxtIMOrg.Location = new System.Drawing.Point(799, 323);
            this.TxtIMOrg.Name = "TxtIMOrg";
            this.TxtIMOrg.Size = new System.Drawing.Size(301, 20);
            this.TxtIMOrg.TabIndex = 21;
            this.TxtIMOrg.TextChanged += new System.EventHandler(this.TxtDefClass_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(796, 307);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "*Item Master Org";
            // 
            // TxtDefClass
            // 
            this.TxtDefClass.Location = new System.Drawing.Point(799, 272);
            this.TxtDefClass.Name = "TxtDefClass";
            this.TxtDefClass.Size = new System.Drawing.Size(301, 20);
            this.TxtDefClass.TabIndex = 19;
            this.TxtDefClass.TextChanged += new System.EventHandler(this.TxtDefClass_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(796, 256);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "*Default Item Class";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 247);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "*Org Code";
            // 
            // TxtOrg
            // 
            this.TxtOrg.Location = new System.Drawing.Point(32, 266);
            this.TxtOrg.Name = "TxtOrg";
            this.TxtOrg.Size = new System.Drawing.Size(85, 20);
            this.TxtOrg.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(274, 247);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "*Material Type";
            // 
            // TxtMatType
            // 
            this.TxtMatType.Location = new System.Drawing.Point(277, 266);
            this.TxtMatType.Name = "TxtMatType";
            this.TxtMatType.Size = new System.Drawing.Size(156, 20);
            this.TxtMatType.TabIndex = 13;
            // 
            // dgMap
            // 
            this.dgMap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMap.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OrgCode,
            this.Product,
            this.MaterialType,
            this.ItemTemplate});
            this.dgMap.Location = new System.Drawing.Point(33, 294);
            this.dgMap.Name = "dgMap";
            this.dgMap.Size = new System.Drawing.Size(715, 144);
            this.dgMap.TabIndex = 11;
            // 
            // OrgCode
            // 
            this.OrgCode.HeaderText = "Org Code";
            this.OrgCode.Name = "OrgCode";
            this.OrgCode.ReadOnly = true;
            this.OrgCode.Width = 120;
            // 
            // Product
            // 
            this.Product.HeaderText = "Product";
            this.Product.Name = "Product";
            this.Product.ReadOnly = true;
            this.Product.Width = 150;
            // 
            // MaterialType
            // 
            this.MaterialType.HeaderText = "Material Type";
            this.MaterialType.Name = "MaterialType";
            this.MaterialType.ReadOnly = true;
            this.MaterialType.Width = 150;
            // 
            // ItemTemplate
            // 
            this.ItemTemplate.HeaderText = "Item Template";
            this.ItemTemplate.Name = "ItemTemplate";
            this.ItemTemplate.ReadOnly = true;
            this.ItemTemplate.Width = 250;
            // 
            // TxtTemplate
            // 
            this.TxtTemplate.Location = new System.Drawing.Point(439, 266);
            this.TxtTemplate.Name = "TxtTemplate";
            this.TxtTemplate.Size = new System.Drawing.Size(228, 20);
            this.TxtTemplate.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(436, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "*Item Template";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(771, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Password";
            // 
            // TxtPass
            // 
            this.TxtPass.Location = new System.Drawing.Point(774, 28);
            this.TxtPass.Name = "TxtPass";
            this.TxtPass.Size = new System.Drawing.Size(245, 20);
            this.TxtPass.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(544, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Username";
            // 
            // TxtUser
            // 
            this.TxtUser.Location = new System.Drawing.Point(547, 28);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.Size = new System.Drawing.Size(221, 20);
            this.TxtUser.TabIndex = 8;
            // 
            // ProcessTypes
            // 
            this.ProcessTypes.Controls.Add(this.BtnDelPType);
            this.ProcessTypes.Controls.Add(this.dgDetails);
            this.ProcessTypes.Controls.Add(this.dgProcessTypes);
            this.ProcessTypes.Controls.Add(this.BtnDown);
            this.ProcessTypes.Controls.Add(this.BtnUp);
            this.ProcessTypes.Controls.Add(this.BtnDelPDet);
            this.ProcessTypes.Controls.Add(this.button5);
            this.ProcessTypes.Controls.Add(this.label13);
            this.ProcessTypes.Controls.Add(this.TxtEnd);
            this.ProcessTypes.Controls.Add(this.label14);
            this.ProcessTypes.Controls.Add(this.TxtStart);
            this.ProcessTypes.Controls.Add(this.TxtPType);
            this.ProcessTypes.Controls.Add(this.label15);
            this.ProcessTypes.Location = new System.Drawing.Point(4, 22);
            this.ProcessTypes.Name = "ProcessTypes";
            this.ProcessTypes.Padding = new System.Windows.Forms.Padding(3);
            this.ProcessTypes.Size = new System.Drawing.Size(1145, 477);
            this.ProcessTypes.TabIndex = 1;
            this.ProcessTypes.Text = "Process Types";
            this.ProcessTypes.UseVisualStyleBackColor = true;
            // 
            // BtnDelPType
            // 
            this.BtnDelPType.Location = new System.Drawing.Point(567, 145);
            this.BtnDelPType.Name = "BtnDelPType";
            this.BtnDelPType.Size = new System.Drawing.Size(75, 23);
            this.BtnDelPType.TabIndex = 39;
            this.BtnDelPType.Text = "Delete";
            this.BtnDelPType.UseVisualStyleBackColor = true;
            this.BtnDelPType.Click += new System.EventHandler(this.BtnDelPType_Click);
            // 
            // dgDetails
            // 
            this.dgDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDetails.Location = new System.Drawing.Point(683, 87);
            this.dgDetails.Name = "dgDetails";
            this.dgDetails.Size = new System.Drawing.Size(366, 374);
            this.dgDetails.TabIndex = 38;
            // 
            // dgProcessTypes
            // 
            this.dgProcessTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProcessTypes.Location = new System.Drawing.Point(15, 87);
            this.dgProcessTypes.Name = "dgProcessTypes";
            this.dgProcessTypes.Size = new System.Drawing.Size(546, 374);
            this.dgProcessTypes.TabIndex = 37;
            // 
            // BtnDown
            // 
            this.BtnDown.Location = new System.Drawing.Point(567, 116);
            this.BtnDown.Name = "BtnDown";
            this.BtnDown.Size = new System.Drawing.Size(75, 23);
            this.BtnDown.TabIndex = 36;
            this.BtnDown.Text = "Down";
            this.BtnDown.UseVisualStyleBackColor = true;
            // 
            // BtnUp
            // 
            this.BtnUp.Location = new System.Drawing.Point(567, 87);
            this.BtnUp.Name = "BtnUp";
            this.BtnUp.Size = new System.Drawing.Size(75, 23);
            this.BtnUp.TabIndex = 35;
            this.BtnUp.Text = "Up";
            this.BtnUp.UseVisualStyleBackColor = true;
            // 
            // BtnDelPDet
            // 
            this.BtnDelPDet.Location = new System.Drawing.Point(1055, 87);
            this.BtnDelPDet.Name = "BtnDelPDet";
            this.BtnDelPDet.Size = new System.Drawing.Size(75, 23);
            this.BtnDelPDet.TabIndex = 34;
            this.BtnDelPDet.Text = "Delete";
            this.BtnDelPDet.UseVisualStyleBackColor = true;
            this.BtnDelPDet.Click += new System.EventHandler(this.BtnDelPDet_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(567, 41);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 33;
            this.button5.Text = "Add";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.BtnAddPType_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(165, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "*Number Starts With";
            // 
            // TxtEnd
            // 
            this.TxtEnd.Location = new System.Drawing.Point(384, 43);
            this.TxtEnd.Name = "TxtEnd";
            this.TxtEnd.Size = new System.Drawing.Size(177, 20);
            this.TxtEnd.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(381, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "*Number Ends With";
            // 
            // TxtStart
            // 
            this.TxtStart.Location = new System.Drawing.Point(168, 43);
            this.TxtStart.Name = "TxtStart";
            this.TxtStart.Size = new System.Drawing.Size(177, 20);
            this.TxtStart.TabIndex = 28;
            // 
            // TxtPType
            // 
            this.TxtPType.Location = new System.Drawing.Point(15, 43);
            this.TxtPType.Name = "TxtPType";
            this.TxtPType.Size = new System.Drawing.Size(117, 20);
            this.TxtPType.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "*Process Type";
            // 
            // label12
            // 
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Location = new System.Drawing.Point(-9, 538);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(1259, 2);
            this.label12.TabIndex = 27;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(1037, 549);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(131, 23);
            this.BtnCancel.TabIndex = 28;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // FormConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1177, 581);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TabCtl);
            this.Controls.Add(this.BtnSave);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConfig";
            this.Text = "Modify Fusion Environments";
            this.Load += new System.EventHandler(this.FormConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgEnv)).EndInit();
            this.TabCtl.ResumeLayout(false);
            this.EnvDetails.ResumeLayout(false);
            this.EnvDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMap)).EndInit();
            this.ProcessTypes.ResumeLayout(false);
            this.ProcessTypes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgProcessTypes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgEnv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtName;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.TextBox TxtURL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.TabControl TabCtl;
        private System.Windows.Forms.TabPage EnvDetails;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtPass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtUser;
        private System.Windows.Forms.TabPage ProcessTypes;
        private System.Windows.Forms.Button BtnAddMap;
        private System.Windows.Forms.TextBox TxtDefTemplate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtIMOrg;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtDefClass;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtOrg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtMatType;
        private System.Windows.Forms.DataGridView dgMap;
        private System.Windows.Forms.TextBox TxtTemplate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnDelMap;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnDown;
        private System.Windows.Forms.Button BtnUp;
        private System.Windows.Forms.Button BtnDelPDet;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtEnd;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox TxtStart;
        private System.Windows.Forms.TextBox TxtPType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dgProcessTypes;
        private System.Windows.Forms.DataGridView dgDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn Environment;
        private System.Windows.Forms.DataGridViewTextBoxColumn URL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Credentials;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox TxtProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrgCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemTemplate;
        private System.Windows.Forms.Button BtnDelPType;
    }
}