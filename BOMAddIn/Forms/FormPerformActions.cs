﻿using Insight.Fusion.Utils;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Insight.Fusion.BOMAddIn
{
    public partial class FormPerformActions : Form, IProgressTracker
    {
        private FusionRibbon Ribbon;

        public FormPerformActions(FusionRibbon ribbon)
        {
            InitializeComponent();
            this.Ribbon = ribbon;
        }

        public void Init(string description, int total)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((Action)delegate
                {
                    if (!String.IsNullOrWhiteSpace(description))
                        Text = description;
                });
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(description))
                    Text = description;
            }

            if (BtnClose.InvokeRequired)
            {
                BtnClose.Invoke((Action)delegate
                {
                    BtnClose.Enabled = false;
                });
            }
            else
            {
                BtnClose.Enabled = false;
            }

            if (progressBar.InvokeRequired)
            {
                progressBar.Invoke((Action)delegate
                {
                    progressBar.Style = ProgressBarStyle.Blocks;
                    progressBar.Minimum = 0;
                    progressBar.Maximum = total;

                    progressBar.Value = 0;
                });
            }
            else
            {
                progressBar.Style = ProgressBarStyle.Blocks;
                progressBar.Minimum = 0;
                progressBar.Maximum = total;

                progressBar.Value = 0;
            }

            if (dgLog.InvokeRequired)
            {
                dgLog.Invoke((Action)delegate
                {
                    dgLog.Rows.Clear();
                });
            }
            else
            {
                dgLog.Rows.Clear();
            }

            Ribbon.ToggleButtons(false);

            if (this.InvokeRequired)
            {
                this.Invoke((Action)delegate
                {
                    this.Show();
                });
            }
            else
            {
                this.Show();
            }
        }

        public void OnCompletion()
        {

        }

        public void OnFinalCompletion()
        {
            if (CirProgressBar.InvokeRequired)
            {
                CirProgressBar.Invoke((Action)delegate
                {
                    CirProgressBar.Value = 100;
                    CirProgressBar.Text = "Done";
                    CirProgressBar.Style = ProgressBarStyle.Continuous;
                });
            }
            else
            {
                CirProgressBar.Value = 100;
                CirProgressBar.Text = "Done";
                CirProgressBar.Style = ProgressBarStyle.Continuous;
            }

            if (BtnClose.InvokeRequired)
            {
                BtnClose.Invoke((Action)delegate
                {
                    BtnClose.Enabled = true;
                });
            }
            else
            {
                BtnClose.Enabled = true;
            }
        }

        public void AddMessage(string message, int imageIdx)
        {
            Image image;
            switch (imageIdx)
            {
                case 1:
                    image = (Image)Properties.Resources.delete;
                    break;
                case 2:
                    image = (Image)Properties.Resources.tick;
                    break;
                default:
                    image = (Image)Properties.Resources.task;
                    break;
            }

            if (dgLog.InvokeRequired)
            {
                dgLog.Invoke((Action)delegate
                {
                    dgLog.Rows.Add(image, message);
                });
            }
            else
            {
                dgLog.Rows.Add(image, message);
            }
        }

        public void SetProgress(int current, int total)
        {
            if (progressBar.InvokeRequired)
            {
                progressBar.Invoke((Action)delegate
                {
                    progressBar.Value = current < total ? current : total;
                });
            }
            else
            {
                progressBar.Value = current < total ? current : total;
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Ribbon.ToggleButtons(true);
            if (this.InvokeRequired)
            {
                this.Invoke((Action)delegate
                {
                    this.Hide();
                });
            }
            else
            {
                Hide();
            }
        }
    }
}
