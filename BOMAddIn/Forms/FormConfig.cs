﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Insight.Fusion.Utils;

namespace Insight.Fusion.BOMAddIn
{
    public partial class FormConfig : Form
    {
        private FusionRibbon Ribbon;
        private DataTable dtPTypes;
        private DataTable dtPTypeDetail;
        private DataSet dataSet;

        public FormConfig(FusionRibbon ribbon)
        {
            InitializeComponent();

            dtPTypes = new DataTable("dtPTypes");
            var column = new DataColumn { ColumnName = "ProcessType", DataType = typeof(string), Caption = "Process Type" };
            dtPTypes.Columns.Add(column);
            dtPTypes.Columns.Add("Pattern", typeof(string));
            dtPTypes.PrimaryKey = new DataColumn[] { column };

            dtPTypeDetail = new DataTable("dtPTypeDetail");
            dtPTypeDetail.Columns.Add(new DataColumn { ColumnName = "ProcessType", DataType = typeof(string), Caption = "Process Type" });
            dtPTypeDetail.Columns.Add(new DataColumn { ColumnName = "StartsWith", DataType = typeof(string), Caption = "Starts With" });
            dtPTypeDetail.Columns.Add(new DataColumn { ColumnName = "EndsWith", DataType = typeof(string), Caption = "Ends With" });

            this.Ribbon = ribbon;

            Ribbon.EnvList.ForEach(e => dgEnv.Rows.Add(new String[] { e.Alias, e.Url.ToString(), e.Credentials }));
            Ribbon.Config.templates.ForEach(t => dgMap.Rows.Add(new String[] { t.OrgCode, t.Product, t.MaterialType, t.ItemTemplate }));

            TxtDefClass.Text = Ribbon.Config.DefaultItemClass;
            TxtIMOrg.Text = Ribbon.Config.ItemMasterOrg;
            TxtDefTemplate.Text = Ribbon.Config.DefaultItemTemplate;

            Ribbon.Config.ProcessTypes.ForEach(t =>
            {
                dtPTypes.Rows.Add(t.Code, t.Pattern);
                t.Pattern.Substring(1, t.Pattern.Length - 2).Split('|').ToList()
                .ForEach(p =>
                {
                    string[] pattern = p.Replace("^", string.Empty).Replace("$", string.Empty).Split(new string[] { ".*" }, StringSplitOptions.None);
                    string start = pattern[0];
                    string end = pattern.Length > 1 ? pattern[1] : "";

                    dtPTypeDetail.Rows.Add(t.Code, start, end);
                });
            });

            dataSet = new DataSet();
            dataSet.Tables.Add(dtPTypes);
            dataSet.Tables.Add(dtPTypeDetail);

            dataSet.Relations.Add("Relation", dataSet.Tables[0].Columns[0], dataSet.Tables[1].Columns[0]);

            var bsPTypes = new BindingSource { DataSource = dtPTypes };

            BindingSource bsPTypeDetail = new BindingSource { DataSource = bsPTypes, DataMember = "Relation" };

            dgProcessTypes.DataSource = bsPTypes;
            dgProcessTypes.Columns[1].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dgProcessTypes.Columns[1].Width = 250;
            dgProcessTypes.ReadOnly = true;

            dgDetails.DataSource = bsPTypeDetail;
            dgDetails.ReadOnly = true;

            BtnSave.Enabled = false;
        }

        private void FormConfig_Closing(object sender, CancelEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                DialogResult result = MessageBox.Show("Do you want to save pending changes?", "Confirmation", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    BtnSave_Click(sender, e);
                }
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TxtName.Text) || String.IsNullOrEmpty(TxtURL.Text))
            {
                MessageBox.Show("Please provide both name and url for registering new environment.");
            }
            else if (Uri.IsWellFormedUriString(TxtURL.Text, UriKind.Absolute))
            {
                FusionEnv env = new FusionEnv { Alias = TxtName.Text }.SetUrl(TxtURL.Text).SetCredentials(TxtUser.Text, TxtPass.Text);
                dgEnv.Rows.Add(new String[] { env.Alias, env.Url.ToString(), env.Credentials });
                BtnSave.Enabled = true;
            }
            else
            {
                MessageBox.Show("Invalid URL specified, please provide a valid url to register the environment.");
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            String errMsg = "Failed to save fusion environment configuration. Cause: {0}";
            try
            {
                List<FusionEnv> newEnvList = new List<FusionEnv>();

                foreach (DataGridViewRow row in dgEnv.Rows)
                {
                    String alias = row.Cells[0]?.Value?.ToString();
                    String url = row.Cells[1]?.Value?.ToString();
                    String credentials = row.Cells[2]?.Value?.ToString();
                    if (!String.IsNullOrWhiteSpace(alias))
                    {

                        FusionEnv fusionEnv = Ribbon.EnvList.Find(env => env.Alias == alias)?.SetUrl(url)?.SetCredentials(credentials) ??
                             new FusionEnv { Alias = alias }.SetUrl(url).SetCredentials(credentials);
                        newEnvList.Add(fusionEnv);
                    }
                }
                Ribbon.EnvList = newEnvList;
                if (!Ribbon.EnvList.Contains(Ribbon.Env))
                    Ribbon.Env = Ribbon.EnvList[0];

                FusionEnv.SaveToFile(Ribbon.EnvList);

                List<TemplateMap> templates = new List<TemplateMap>();
                foreach (DataGridViewRow row in dgMap.Rows)
                {
                    String orgCode = row.Cells[0]?.Value?.ToString();
                    String product = row.Cells[1]?.Value?.ToString();
                    String matType = row.Cells[2]?.Value?.ToString();
                    String itmTmp = row.Cells[3]?.Value?.ToString();

                    if (!String.IsNullOrWhiteSpace(orgCode))
                    {
                        TemplateMap templateMap = new TemplateMap { OrgCode = orgCode, Product = product, MaterialType = matType, ItemTemplate = itmTmp };
                        templates.Add(templateMap);
                    }
                }
                Ribbon.Config.templates = templates;

                List<ProcessType> processTypes = new List<ProcessType>();
                foreach (DataGridViewRow row in dgProcessTypes.Rows)
                {
                    String code = row.Cells[0]?.Value?.ToString();
                    String pattern = row.Cells[1]?.Value?.ToString();

                    if (!String.IsNullOrWhiteSpace(code))
                    {
                        ProcessType processType = new ProcessType { Code = code, Pattern = pattern };
                        processTypes.Add(processType);
                    }
                }
                Ribbon.Config.ProcessTypes = processTypes;

                Ribbon.Config.DefaultItemClass = TxtDefClass.Text;
                Ribbon.Config.ItemMasterOrg = TxtIMOrg.Text;
                Ribbon.Config.DefaultItemTemplate = TxtDefTemplate.Text;

                Config.SaveToFile(Ribbon.Config);
                BtnSave.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format(errMsg, ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (dgEnv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in dgEnv.SelectedRows)
                {
                    dgEnv.Rows.Remove(row);
                    BtnSave.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Please select the row you want to remove.");
            }
        }

        private void FormConfig_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Hide();
            Close();
        }

        private void BtnAddMap_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TxtOrg.Text) || String.IsNullOrEmpty(TxtMatType.Text) ||
                String.IsNullOrEmpty(TxtProduct.Text) || String.IsNullOrEmpty(TxtTemplate.Text))
            {
                MessageBox.Show("Please provide org code, product, material type and template values to create a new mapping.");
            }

            dgMap.Rows.Add(new String[] { TxtOrg.Text, TxtProduct.Text, TxtMatType.Text, TxtTemplate.Text });
            BtnSave.Enabled = true;
        }

        private void BtnDelMap_Click(object sender, EventArgs e)
        {
            if (dgMap.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in dgMap.SelectedRows)
                {
                    dgMap.Rows.Remove(row);
                    BtnSave.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Please select the row you want to remove.");
            }
        }

        private void TxtDefClass_TextChanged(object sender, EventArgs e)
        {
            BtnSave.Enabled = true;
        }

        private void BtnAddPType_Click(object sender, EventArgs e)
        {
            var pType = TxtPType.Text;
            var starts = TxtStart.Text;
            var ends = TxtEnd.Text;

            if (String.IsNullOrWhiteSpace(pType))
            {
                MessageBox.Show("Process Type code must be specified.");
                return;
            }

            if (String.IsNullOrWhiteSpace(starts) && String.IsNullOrWhiteSpace(ends))
            {
                MessageBox.Show("Starts With or Ends With pattern must be specified.");
                return;
            }

            DataRow row = dtPTypes.Rows.Find(pType);
            if (row == null)
            {
                var pattern = "(" + (!String.IsNullOrWhiteSpace(starts) ? "^" + starts : "") + (!String.IsNullOrWhiteSpace(ends) ? ".*" + ends + "$" : "") + ")";
                dtPTypes.Rows.Add(new String[] { pType, pattern });
                dtPTypeDetail.Rows.Add(new String[] { pType, starts, ends });
                BtnSave.Enabled = true;
            }
            else
            {
                DataRow[] rows = dtPTypeDetail.Select(String.Format("ProcessType = '{0}' AND StartsWith = '{1}' AND EndsWith = '{2}'", pType, starts, ends));
                if (rows.Length > 0)
                {
                    MessageBox.Show("Combination already exists.");
                }
                else
                {
                    dtPTypeDetail.Rows.Add(new String[] { pType, starts, ends });
                    var pattern = (!String.IsNullOrWhiteSpace(starts) ? "^" + starts : "") + (!String.IsNullOrWhiteSpace(ends) ? ".*" + ends + "$" : "");
                    var currentPattern = row.Field<String>(1);
                    row.SetField(1, currentPattern.Substring(0, currentPattern.Length - 1) + "|" + pattern + ")");
                    BtnSave.Enabled = true;
                }
            }
        }

        private void BtnDelPType_Click(object sender, EventArgs e)
        {
            if (dgProcessTypes.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in dgProcessTypes.SelectedRows)
                {
                    string pType = row.Cells[0].Value.ToString();
                    dtPTypes.Rows.Remove(dtPTypes.Rows.Find(pType));

                    DataRow[] rows = dtPTypeDetail.Select(String.Format("ProcessType = '{0}'", pType));
                    foreach (DataRow dRow in rows)
                    {
                        dtPTypeDetail.Rows.Remove(dRow);
                    }
                }
                BtnSave.Enabled = true;
            }
            else
            {
                MessageBox.Show("Please select the row you want to remove.");
            }
        }

        private void BtnDelPDet_Click(object sender, EventArgs e)
        {
            if (dgDetails.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in dgDetails.SelectedRows)
                {
                    string pType = row.Cells[0].Value.ToString();
                    string starts = row.Cells[1].Value.ToString();
                    string ends = row.Cells[2].Value.ToString();

                    DataRow[] rows = dtPTypeDetail.Select(String.Format("ProcessType = '{0}' AND StartsWith = '{1}' AND EndsWith = '{2}'", pType, starts, ends));
                    foreach (DataRow dRow in rows)
                    {
                        dtPTypeDetail.Rows.Remove(dRow);
                    }
                    DataRow[] rowsLeft = dtPTypeDetail.Select(String.Format("ProcessType = '{0}'", pType));
                    var pattern =  String.Join("|" , rowsLeft.Select(r => (!String.IsNullOrWhiteSpace(r.Field<String>(1)) ? "^" + r.Field<String>(1) : "") +
                        (!String.IsNullOrWhiteSpace(r.Field<String>(2)) ? ".*" + r.Field<String>(2) + "$" : "")
                    ).ToList());
                    
                    DataRow rowParent = dtPTypes.Rows.Find(pType);
                    rowParent.SetField(1, "(" + pattern + ")");

                    BtnSave.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Please select the row you want to remove.");
            }
        }
    }
}
