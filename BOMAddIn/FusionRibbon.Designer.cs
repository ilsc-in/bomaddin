﻿namespace Insight.Fusion.BOMAddIn
{
    partial class FusionRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public FusionRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FusionRibbon));
            this.tab1 = this.Factory.CreateRibbonTab();
            this.bomTab = this.Factory.CreateRibbonTab();
            this.grpConfig = this.Factory.CreateRibbonGroup();
            this.BtnLogin = this.Factory.CreateRibbonButton();
            this.BtnConfig = this.Factory.CreateRibbonButton();
            this.grpBOMActions = this.Factory.CreateRibbonGroup();
            this.BtnTemplate = this.Factory.CreateRibbonButton();
            this.BtnValidate = this.Factory.CreateRibbonButton();
            this.BtnItems = this.Factory.CreateRibbonButton();
            this.BtnBOM = this.Factory.CreateRibbonButton();
            this.BtnWD = this.Factory.CreateRibbonButton();
            this.button1 = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.bomTab.SuspendLayout();
            this.grpConfig.SuspendLayout();
            this.grpBOMActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // bomTab
            // 
            this.bomTab.Groups.Add(this.grpConfig);
            this.bomTab.Groups.Add(this.grpBOMActions);
            this.bomTab.Label = "BOM";
            this.bomTab.Name = "bomTab";
            // 
            // grpConfig
            // 
            this.grpConfig.Items.Add(this.BtnLogin);
            this.grpConfig.Items.Add(this.BtnConfig);
            this.grpConfig.Label = "Config";
            this.grpConfig.Name = "grpConfig";
            // 
            // BtnLogin
            // 
            this.BtnLogin.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnLogin.Image = ((System.Drawing.Image)(resources.GetObject("BtnLogin.Image")));
            this.BtnLogin.Label = "Login";
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.OfficeImageId = "Login";
            this.BtnLogin.ShowImage = true;
            this.BtnLogin.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnLogin_Click);
            // 
            // BtnConfig
            // 
            this.BtnConfig.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnConfig.Image = global::Insight.Fusion.BOMAddIn.Properties.Resources.settings;
            this.BtnConfig.Label = "Config";
            this.BtnConfig.Name = "BtnConfig";
            this.BtnConfig.ShowImage = true;
            this.BtnConfig.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnConfig_Click);
            // 
            // grpBOMActions
            // 
            this.grpBOMActions.Items.Add(this.BtnTemplate);
            this.grpBOMActions.Items.Add(this.BtnValidate);
            this.grpBOMActions.Items.Add(this.BtnItems);
            this.grpBOMActions.Items.Add(this.BtnBOM);
            this.grpBOMActions.Items.Add(this.BtnWD);
            this.grpBOMActions.Items.Add(this.button1);
            this.grpBOMActions.Label = "Actions";
            this.grpBOMActions.Name = "grpBOMActions";
            // 
            // BtnTemplate
            // 
            this.BtnTemplate.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnTemplate.Image = ((System.Drawing.Image)(resources.GetObject("BtnTemplate.Image")));
            this.BtnTemplate.Label = "Prepare Template";
            this.BtnTemplate.Name = "BtnTemplate";
            this.BtnTemplate.ShowImage = true;
            this.BtnTemplate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnTemplate_Click);
            // 
            // BtnValidate
            // 
            this.BtnValidate.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnValidate.Image = ((System.Drawing.Image)(resources.GetObject("BtnValidate.Image")));
            this.BtnValidate.Label = "Validate";
            this.BtnValidate.Name = "BtnValidate";
            this.BtnValidate.ShowImage = true;
            this.BtnValidate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnValidate_Click);
            // 
            // BtnItems
            // 
            this.BtnItems.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnItems.Image = global::Insight.Fusion.BOMAddIn.Properties.Resources.export;
            this.BtnItems.Label = "Create Items";
            this.BtnItems.Name = "BtnItems";
            this.BtnItems.ShowImage = true;
            this.BtnItems.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnItems_Click);
            // 
            // BtnBOM
            // 
            this.BtnBOM.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnBOM.Image = ((System.Drawing.Image)(resources.GetObject("BtnBOM.Image")));
            this.BtnBOM.Label = "Create Structures";
            this.BtnBOM.Name = "BtnBOM";
            this.BtnBOM.ShowImage = true;
            this.BtnBOM.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnBOM_Click);
            // 
            // BtnWD
            // 
            this.BtnWD.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.BtnWD.Image = ((System.Drawing.Image)(resources.GetObject("BtnWD.Image")));
            this.BtnWD.Label = "Create Work Definitions";
            this.BtnWD.Name = "BtnWD";
            this.BtnWD.ShowImage = true;
            this.BtnWD.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnWD_Click);
            // 
            // button1
            // 
            this.button1.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.button1.Enabled = false;
            this.button1.Image = global::Insight.Fusion.BOMAddIn.Properties.Resources.tasks;
            this.button1.Label = "Perform All Actions";
            this.button1.Name = "button1";
            this.button1.ShowImage = true;
            this.button1.Visible = false;
            this.button1.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.BtnActions_Click);
            // 
            // FusionRibbon
            // 
            this.Name = "FusionRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Tabs.Add(this.bomTab);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.bomTab.ResumeLayout(false);
            this.bomTab.PerformLayout();
            this.grpConfig.ResumeLayout(false);
            this.grpConfig.PerformLayout();
            this.grpBOMActions.ResumeLayout(false);
            this.grpBOMActions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab bomTab;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpConfig;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnLogin;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnConfig;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpBOMActions;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnTemplate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnValidate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnItems;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnBOM;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton BtnWD;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button1;
    }

    partial class ThisRibbonCollection
    {
        internal FusionRibbon FusionRibbon
        {
            get { return this.GetRibbon<FusionRibbon>(); }
        }
    }
}
