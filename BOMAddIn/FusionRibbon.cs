﻿using System.Collections.Generic;
using Insight.Fusion.Utils;
using Microsoft.Office.Tools.Ribbon;
using System;
using Serilog;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Insight.Fusion.BOMAddIn
{
    public partial class FusionRibbon
    {
        public List<FusionEnv> EnvList;
        public FusionEnv Env;
        public BOMClient Client;

        public bool Authenticated;
        public bool Validated;
        public bool DataRead;

        public Config Config;

        public List<Item> Items;
        public List<Structure> Structures;
        public string OrgCode;
        public string StrName;
        public string Product;
        public string EffType;

        public bool MultiActions;

        private static ILogger log = LogManager.log;

        private void BtnConfig_Click(object sender, RibbonControlEventArgs e)
        {
            if (Initialize())
            {
                FormConfig form = new FormConfig(this);
                ToggleButtons(false);
                form.Show();
            }
        }

        public void ToggleButtons(bool state)
        {
            BtnLogin.Enabled = state;
            BtnConfig.Enabled = state;
            BtnTemplate.Enabled = state;
            BtnValidate.Enabled = state;
            BtnItems.Enabled = state;
            BtnBOM.Enabled = state;
            BtnWD.Enabled = state;
        }

        private void BtnLogin_Click(object sender, RibbonControlEventArgs e)
        {
            if (Initialize())
            {
                FormLogin frm = new FormLogin(this);
                ToggleButtons(false);
                frm.Show();
            }
        }

        private void BtnTemplate_Click(object sender, RibbonControlEventArgs e)
        {
            XLActions.SetupLoadSheet();
        }

        private async void BtnValidate_Click(object sender, RibbonControlEventArgs e)
        {
            MultiActions = false;

            if (!Authenticated)
            {
                ShowMessage("You are currently not connected to Fusion SCM. Please login into SCM before performing this action.", true);
                return;
            }

            Client.Tracker = Client.FormProgress;
            await Validate();
        }

        private async Task<bool> Validate()
        {
            try
            {
                XLActions.ResetFormats();
                if (Authenticated)
                {
                    DateTime from = DateTime.Now;

                    Client.Tracker.Init("Performing Validations", 100);
                    Client.Tracker.SetProgress(10, 100);

                    await Task.Run(() => { XLActions.ReadDataFromSheet(this); });
                    Client.Tracker.SetProgress(20, 100);
                    if (DataRead)
                    {
                        await Task.Run(() => (Client.CheckIfStructuresExistInOrg(OrgCode, Structures)));
                        Client.Tracker.SetProgress(30, 100);

                        await Task.Run(() => (Client.CheckIfItemsExistInOrg(Config.ItemMasterOrg, Items.FindAll(i => i.OrgCode == Config.ItemMasterOrg))));
                        Client.Tracker.SetProgress(50, 100);

                        await Task.Run(() => (Client.CheckIfItemsExistInOrg(OrgCode, Items.FindAll(i => i.OrgCode == OrgCode))));
                        Client.Tracker.SetProgress(70, 100);

                        List<String> missingItems = Items.FindAll(i => !i.Exists).Select(i => i.ItemNumber).Distinct().ToList();

                        await Task.Run(() => { XLActions.FindAndHighlightCells("A6", "A" + (Items.Count / 2 + 5), missingItems, "Bad"); });
                        await Task.Run(() => { XLActions.FindAndHighlightCells("F6", "F" + (Items.Count / 2 + 5), missingItems, "Bad"); });
                        Client.Tracker.SetProgress(80, 100);

                        List<String> existingBOMs = Structures.FindAll(s => s.Exists).Select(s => s.ParentItem).ToList();
                        await Task.Run(() => { XLActions.FindAndHighlightCells("A6", "A" + (Items.Count / 2 + 5), existingBOMs, "Accent2"); });
                        Client.Tracker.SetProgress(90, 100);

                        List<String> badPTypes = Structures.FindAll(s => s.BadPType).Select(s => s.ParentItem).ToList();
                        await Task.Run(() => { XLActions.FindAndHighlightCells("A6", "A" + (Items.Count / 2 + 5), badPTypes, "Accent2"); });
                        Client.Tracker.SetProgress(100, 100);

                        Validated = true;
                        TimeSpan span = DateTime.Now - from;
                        ShowMessage(String.Format("Validation completed in {0} minute, {1} seconds.", span.Minutes, span.Seconds), false, true);
                        return true;
                    }
                    return false;
                }
                else
                {
                    ShowMessage("You are currently not connected to Fusion SCM. Please login into SCM before performing this action.", true);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Failed to perform validation on input data. " + ex.Message, true);
                return false;
            }
            finally
            {
                Client?.Tracker?.OnCompletion();
            }
        }

        private bool Initialize()
        {
            string cfgFile = Environment.GetEnvironmentVariable("BOM_CFG_FILE") ?? Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\bom_config.xml";

            if (cfgFile == null || !new FileInfo(cfgFile).Exists)
            {
                MessageBox.Show(String.Format("Configuration file {0} is missing. Fusion AddIn can't be started", cfgFile));
                return false;
            }

            try
            {
                EnvList = FusionEnv.Load();
                Config = Config.Load(cfgFile);

                if (EnvList?.Count > 0)
                    Env = EnvList[0];
                Client = new BOMClient { Env = Env, FormProgress = new FormProgress(this), FormActions = new FormPerformActions(this) };

                Items = new List<Item>();
                Structures = new List<Structure>();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Failed to load configuration from file {0}. Fusion AddIn can't be started. {1}", cfgFile, ex.Message));
                return false;
            }
        }

        public void Reset()
        {
            OrgCode = "";
            StrName = "";
            EffType = "";

            Items.Clear();
            Structures.Clear();

            Validated = false;
            DataRead = false;
        }

        private async void BtnItems_Click(object sender, RibbonControlEventArgs e)
        {
            MultiActions = false;

            if (!Authenticated)
            {
                ShowMessage("You are currently not connected to Fusion SCM. Please login into SCM before performing this action.", true);
                return;
            }
            if (!Validated)
            {
                ShowMessage("Please perform validations on input data before attempting to create items.", true);
                return;
            }

            Client.Tracker = Client.FormProgress;
            await CreateItems();
        }

        private async Task<bool> CreateItems()
        {
            try
            {
                DateTime from = DateTime.Now;
                if (!Authenticated)
                {
                    ShowMessage("You are currently not connected to Fusion SCM. Please login into SCM before performing this action.", true);
                    return false;
                }
                if (!Validated)
                {
                    ShowMessage("Please perform validations on input data before attempting to create items.", true);
                    return false;
                }

                List<Item> itemsInMasterOrg = Items.FindAll(i => i.OrgCode == Config.ItemMasterOrg);
                try
                {
                    await Task.Run(() => { Client.CreateMissingItems(itemsInMasterOrg); });
                    int missingCount = itemsInMasterOrg.Count(i => !i.Exists);
                    if (missingCount > 0)
                        throw new Exception("Failed to create" + missingCount + " items in org " + Config.ItemMasterOrg + ".");
                }
                catch (Exception ex)
                {
                    ShowMessage("Failed while creating items in org " + Config.ItemMasterOrg + ". " + ex.Message + " Check log file for detailed error messages.", true);
                    return false;
                }

                List<Item> itemsInInvOrg = Items.FindAll(i => i.OrgCode == OrgCode);
                try
                {
                    await Task.Run(() => { Client.CreateMissingItems(itemsInInvOrg); });

                    int missingCount = itemsInInvOrg.Count(i => !i.Exists);
                    if (missingCount > 0)
                        throw new Exception("Failed to create" + missingCount + " items in org " + OrgCode + ".");
                }
                catch (Exception ex)
                {
                    ShowMessage("Failed while creating items in org " + OrgCode + ". " + ex.Message + " Check log file for detailed error messages.", true);
                    return false;
                }

                DateTime to = DateTime.Now;
                TimeSpan span = to - from;

                ShowMessage(String.Format("Item creation finished in {0} min and {1} sec.", span.Minutes, span.Seconds), false, true);
                return true;
            }
            finally
            {
                XLActions.SetStyle("A7", "A" + (Items.Count / 2 + 5));
                XLActions.SetStyle("F7", "F" + (Items.Count / 2 + 5));

                List<String> missingItems = Items.FindAll(i => !i.Exists).Select(i => i.ItemNumber).Distinct().ToList();
                if (missingItems.Count > 0)
                {
                    await Task.Run(() => { XLActions.FindAndHighlightCells("A7", "A" + (Items.Count / 2 + 5), missingItems, "Bad"); });
                    await Task.Run(() => { XLActions.FindAndHighlightCells("F7", "F" + (Items.Count / 2 + 5), missingItems, "Bad"); });
                }

                Client?.Tracker?.OnCompletion();
            }
        }

        private async void BtnBOM_Click(object sender, RibbonControlEventArgs e)
        {
            MultiActions = false;

            if (!Authenticated)
            {
                ShowMessage("You are currently not connected to Fusion SCM. Please login into SCM before performing this action.", true);
                return;
            }
            if (!Validated)
            {
                ShowMessage("Please perform validations on input data before attempting to create items.", true);
                return;
            }

            if (Items.Find(i => !i.Exists) != null)
            {
                ShowMessage("Please perform creation of items before attempting to create structures. All items must already be existing before creating structures.", true);
                return;
            }

            Client.Tracker = Client.FormProgress;
            await CreateBOM();
        }

        private async Task<bool> CreateBOM()
        {
            DateTime from = DateTime.Now;
            if (!Authenticated)
            {
                ShowMessage("You are currently not connected to Fusion SCM. Please login into SCM before performing this action.", true);
                return false;
            }
            if (!Validated)
            {
                ShowMessage("Please perform validations on input data before attempting to create items.", true);
                return false;
            }

            if (Items.Find(i => !i.Exists) != null)
            {
                ShowMessage("Please perform creation of items before attempting to create structures. All items must already be existing before creating structures.", true);
                return false;
            }

            try
            {
                XLActions.UpdateProcessTypes(this);
                await Task.Run(() => { Client.CreateMissingStructures(Structures); });
                int missingCount = Structures.Count(i => !i.Exists);
                if (missingCount > 0)
                    throw new Exception("Failed to create" + missingCount + " structures in org " + OrgCode + ".");
            }
            catch (Exception ex)
            {
                ShowMessage("Failed while creating Structures." + ex.Message, true);
                return false;
            }
            finally
            {
                List<String> existingBOMs = Structures.FindAll(s => s.Exists).Select(s => s.ParentItem).ToList();
                await Task.Run(() => { XLActions.FindAndHighlightCells("A7", "A" + (Items.Count / 2 + 5), existingBOMs, "Accent2"); });

                Client?.Tracker?.OnCompletion();
            }

            DateTime to = DateTime.Now;
            TimeSpan span = to - from;

            ShowMessage(String.Format("Structure creation finished in {0} min and {1} sec.", span.Minutes, span.Seconds), false, true);
            return true;
        }

        private async void BtnWD_Click(object sender, RibbonControlEventArgs e)
        {
            MultiActions = false;

            if (!Authenticated)
            {
                ShowMessage("You are currently not connected to Fusion SCM. Please login into SCM before performing this action.", true);
                return;
            }
            if (!Validated)
            {
                ShowMessage("Please perform validations on input data before attempting to create items.", true);
                return ;
            }

            if (Items.Find(i => !i.Exists) != null)
            {
                ShowMessage("Please perform creation of items before attempting to create Work Definitions. All items must already be existing before creating Work Definitions.", true);
                return ;
            }

            if (Structures.Find(s => !s.Exists) != null)
            {
                ShowMessage("Please perform creation of structures before attempting to create Work Definitions. All structures must already be existing before creating Work Definitions.", true);
                return ;
            }

            Client.Tracker = Client.FormProgress;
            await CreateWD();
        }

        private async Task<bool> CreateWD()
        {
            DateTime from = DateTime.Now;
            if (!Authenticated)
            {
                ShowMessage("You are currently not connected to Fusion SCM. Please login into SCM before performing this action.", true);
                return false;
            }
            if (!Validated)
            {
                ShowMessage("Please perform validations on input data before attempting to create items.", true);
                return false;
            }

            if (Items.Find(i => !i.Exists) != null)
            {
                ShowMessage("Please perform creation of items before attempting to create Work Definitions. All items must already be existing before creating Work Definitions.", true);
                return false;
            }

            if (Structures.Find(s => !s.Exists) != null)
            {
                ShowMessage("Please perform creation of structures before attempting to create Work Definitions. All structures must already be existing before creating Work Definitions.", true);
                return false;
            }

            try
            {
                await Task.Run(() => { Client.CreateWorkDefinitions(OrgCode, Product, Items.FindAll(i => i.OrgCode == OrgCode && i.IsFG)); });
                int missingCount = Items.Count(i => i.IsFG && !i.WDExists && i.OrgCode == OrgCode);
                if (missingCount > 0)
                    throw new Exception("Failed to create " + missingCount + " work definitions in org " + OrgCode + ".");

            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, true);
                return false;
            }
            finally
            {
                List<String> items = Items.FindAll(i => i.Exists && (i.WDExists || !i.IsFG)).Select(i => i.ItemNumber).ToList();
                await Task.Run(() => { XLActions.FindAndHighlightCells("A7", "A" + (Items.Count / 2 + 5), items, "Good"); });

                Client?.Tracker?.OnCompletion();
            }

            DateTime to = DateTime.Now;
            TimeSpan span = to - from;

            ShowMessage(String.Format("Work Definition creation finished in {0} min and {1} sec.", span.Minutes, span.Seconds), false, true);
            return true;
        }

        private async void BtnActions_Click(object sender, RibbonControlEventArgs e)
        {
            if (!Authenticated)
            {
                ShowMessage("You are currently not connected to Fusion SCM. Please login into SCM before performing this action.", true);
                return;
            }
            if (!Validated)
            {
                ShowMessage("Please perform validations on input data before attempting to create items.", true);
                return;
            }

            MultiActions = true;
            Client.Tracker = Client.FormActions;

            bool success = await CreateItems();
            if (success)
                success = await CreateBOM();
            if (success)
                success = await CreateWD();

            Client?.FormActions?.OnFinalCompletion();

            ToggleButtons(true);
        }

        private void ShowMessage(string message, bool error = false, bool success = false)
        {
            if (MultiActions)
            {
                Client?.FormActions?.AddMessage(message, error ? 1 : success ? 2 : 0);
            }
            else
            {
                MessageBox.Show(message);
            }
        }
    }
}
