using System;
using System.Net;
using System.Xml.Linq;
using System.Text;
using System.IO;
using RestSharp;
using Newtonsoft.Json.Linq;
using Insight.Fusion.BOMAddIn;

namespace Insight.Fusion.Utils
{
    public class FusionClient
    {
        public FusionEnv Env { get; set; }
        public IProgressTracker Tracker;
        public FormPerformActions FormActions;
        public FormProgress FormProgress;

        static FusionClient()
        {
            HttpWebRequest.DefaultMaximumErrorResponseLength = Int32.MaxValue;
            ServicePointManager.DefaultConnectionLimit = 20;
            ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls11;
        }

        public static RestRequest BuildRequest(Method method, string endpoint, string fields, string limit, string query, string expand)
        {
            RestRequest request = new RestRequest(endpoint, method);
            if (fields != null)
            {
                request.AddQueryParameter("fields", fields);
            }
            if (expand != null)
            {
                request.AddQueryParameter("expand", expand);
            }
            request.AddQueryParameter("onlyData", "true");
            if (limit != null)
            {
                request.AddQueryParameter("limit", limit);
            }
            if (query != null)
            {
                request.AddQueryParameter("q", query);
            }
            request.AddHeader("content-type", "application/vnd.oracle.adf.resourceitem+json");

            return request;
        }

        public static RestRequest BuildRequest(Method method, string endpoint)
        {
            RestRequest request = new RestRequest(endpoint, method);
            request.AddHeader("content-type", "application/vnd.oracle.adf.resourceitem+json");

            return request;
        }

        public HttpWebRequest BuildRequest(string endpoint, string soapAction, string payload)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(Env.Url, endpoint));
            request.Method = "POST";
            request.Timeout = 4 * 60 * 1000;
            request.ContentType = "text/xml;charset=UTF-8";
            request.Headers.Add("Authorization", Env.GetCredentials());

            if (soapAction == null)
                soapAction = "";
            request.Headers.Add("SOAPAction", soapAction);

            byte[] byteArray = Encoding.UTF8.GetBytes(payload);
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            return request;
        }

        public APIResponse GetResponse(HttpWebRequest request)
        {
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    var status = response.StatusCode;
                    return BuildAPIResponse(response);
                }
            }
            catch (WebException ex)
            {
                HttpWebResponse response = (HttpWebResponse)ex.Response;
                XDocument document = null;
                String text;
                var status = HttpStatusCode.InternalServerError;
                if (response != null)
                {
                    status = response.StatusCode;
                    using (var stream = response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        text = reader.ReadToEnd();
                        document = Extensions.IgnoreErrors<XDocument>(() => XDocument.Parse(text));

                        return new APIResponse { Status = response.StatusCode, Document = document, Trace = ex.Message + "\n" + ex.StackTrace, Text = text };
                    }
                }
                else
                {
                    text = ex.Message;
                }
                return new APIResponse { Status = status, Document = document, Trace = ex.Message + "\n" + ex.StackTrace, Text = text };
            }
            catch (Exception ex)
            {
                return new APIResponse { Status = HttpStatusCode.InternalServerError, Trace = ex.Message + "\n" + ex.StackTrace, Text = ex.Message };
            }
        }

        public APIResponse GetResponse(IRestRequest request)
        {
            RestClient client = new RestClient(Env.Url);
            //RestClient client = new RestClient("http://10.0.0.103:8089/");
            client.AddDefaultHeader("Authorization", Env.GetCredentials());

            var response = client.Execute(request);

            var apiResponse =  new APIResponse
            {
                Status = response.StatusCode,
                JObject = Extensions.IgnoreErrors<JObject>(() => JObject.Parse(response.Content)),
                Text = response.Content
            };

            if (response.ErrorException != null)
            {
                Exception ex = response.ErrorException;
                apiResponse.Payload = request.Parameters.Find(p => p.Type == ParameterType.RequestBody)?.Value?.ToString();
                apiResponse.Trace = ex.Message + "\n" + ex.StackTrace;
            }

            return apiResponse;
        }

        private APIResponse BuildAPIResponse(HttpWebResponse response)
        {
            using (var stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                String text = reader.ReadToEnd();
                if (response.ContentType.Contains("multipart/"))
                    text = text.ExtractSOAPEnvelope();
                try
                {
                    return new APIResponse { Status = response.StatusCode, Document = XDocument.Parse(text) };
                }
                catch (Exception ex)
                {
                    return new APIResponse { Status = response.StatusCode, Trace = ex.Message + "\n" + ex.StackTrace, Text = text };
                }
            }
        }

        public Boolean Authenticate()
        {
            var payload = @"<Envelope xmlns=""http://schemas.xmlsoap.org/soap/envelope/"" 
            xmlns:typ=""http://xmlns.oracle.com/apps/hcm/people/roles/userDetailsServiceV2/types/"">
                <Body>
                    <typ:findSelfUserDetails/>
                </Body>
            </Envelope>";
            var soapAction = "http://xmlns.oracle.com/apps/hcm/people/roles/userDetailsServiceV2/findSelfUserDetails";

            var apiResponse = GetResponse(BuildRequest("hcmService/UserDetailsServiceV2", soapAction, payload));
            return apiResponse.Status == HttpStatusCode.OK;
        }
    }

    public class APIResponse
    {
        public string Text { get; set; }
        public string Trace { get; set; }
        public string Payload { get; set; }
        public JObject JObject { get; set; }
        public XDocument Document { get; set; }
        public HttpStatusCode Status { get; set; }

        public override String ToString()
        {
            return String.Format("Status: {0}, text: {1}, document: {2}, jObject: {3}, trace: {4}, payload: {5}", Status, Text,
                Document?.ToString(), JObject, Trace, Payload);
        }
    }
}