﻿using System;

namespace Insight.Fusion.Utils
{
    public interface IProgressTracker
    {
        void Init(String description, int total);
        void SetProgress(int current, int total);
        void OnCompletion();
    }

    public class ConsoleProgressTracker : IProgressTracker
    {
        public void Init(string description, int total)
        {
            DrawProgressBar(0, total, description);
        }

        public void OnCompletion()
        {

        }

        public void SetProgress(int current, int total)
        {
            DrawProgressBar(current, total);
        }

        private static void DrawProgressBar(int current, int total, string description = "")
        {
            float onechunk = 30.0f / total;
            int progress = (int)(onechunk * current);
            Console.CursorLeft = 0;
            Console.Write("[");
            Console.BackgroundColor = ConsoleColor.Green;
            Console.Write("".PadLeft(progress));
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.Write("".PadLeft(30 - progress));
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("] {0} of {1} {2}   ", current, total, description);
        }
    }
}
