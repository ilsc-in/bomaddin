using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Net.Http;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Insight.Fusion.Utils
{
    public static class Extensions
    {
        public static MultipartContent ReadAsMultipartContent(this string input)
        {
            string boundary = new Regex(@"(.*boundary="")(.*?)"";", RegexOptions.Singleline).Split(input)[2];

            MultipartContent multipart = new MultipartContent("multipart/related", boundary);

            foreach (string content in new Regex(boundary).Split(input))
            {
                multipart.Add(content.ReadAsStreamContent());
            }
            return multipart;
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue defaultVal)
      => dict.TryGetValue(key, out var value) ? value : defaultVal;

        public static string ExtractSOAPEnvelope(this string input)
        {
            Match match = new Regex(@"<.*?envelope.*?>.*</.*?envelope>", RegexOptions.IgnoreCase).Match(input);

            if (match.Success) return match.ToString();
            else throw new ArgumentException("Input string does not contain a SOAP Envelope");
        }
        public static string GetErrors(this XDocument xDoc)
        {
            string faultStr = xDoc?.Descendants()?.Where(elm => elm.Name.LocalName == "faultstring")?.SingleOrDefault()?.Value ?? "";
            MatchCollection mc = new Regex(@"(<TEXT>)(.*?)(</Text>)", RegexOptions.IgnoreCase).Matches(faultStr);
            
            if (mc.Count > 0) return string.Join("\n", mc.Cast<Match>().Select(m => m.Groups[2].ToString()));

            return "";
        }
        public static List<List<T>> ChunkBy<T>(this List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => (int)(x.Index / chunkSize))
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
        private static StreamContent ReadAsStreamContent(this string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return new StreamContent(stream);
        }
        public static T IgnoreErrors<T>(Func<T> operation, T defaultValue = default(T))
        {
            if (operation == null)
                return defaultValue;

            T result;
            try
            {
                result = operation.Invoke();
            }
            catch
            {
                result = defaultValue;
            }

            return result;
        }
    }
}