using System;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;

namespace Insight.Fusion.Utils
{
    public class FusionEnv
    {
        public string Alias { get; set; }
        public Uri Url { get; set; }
        public string Credentials { get; private set; }

        public static List<FusionEnv> Load(String envFile = null)
        {
            envFile = envFile ?? Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\fusion_envs.xml";

            List<FusionEnv> environments = new List<FusionEnv>();

            if (File.Exists(envFile))
            {
                XElement cfgDoc = XElement.Load(envFile);

                environments = cfgDoc
                .Elements("Environment")
                .Select((e) => new FusionEnv
                {
                    Alias = e.Element("Alias").Value,
                    Url = new Uri(e.Element("URL").Value),
                    Credentials = e.Element("Credentials")?.Value
                })
                .ToList();
            }

            return environments;
        }

        public static void SaveToFile(List<FusionEnv> envList)
        {
            String envFile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\fusion_envs.xml";

            String xml = new XElement("Environments",
                from env in envList
                select new XElement("Environment",
                    new XElement("Alias", env.Alias),
                    new XElement("URL", env.Url),
                    new XElement("Credentials", env.Credentials)
                )
            ).ToString();

            File.WriteAllText(envFile, xml);
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public String GetUserName()
        {
            return Credentials != null ? Base64Decode(Credentials).Split(':')[0] : "";
        }

        public String GetPassword()
        {
            return Credentials != null ? Base64Decode(Credentials).Split(':')[1] : "";
        }

        public FusionEnv SetCredentials(String user, String password)
        {
            if (user != null && password != null)
                this.Credentials = Base64Encode(user + ":" + password);
            return this;
        }

        public FusionEnv SetCredentials(String credentials)
        {
            this.Credentials = credentials;
            return this;
        }

        public FusionEnv SetUrl(String url)
        {
            this.Url = new Uri(url);
            return this;
        }

        public FusionEnv SetUrl(Uri url)
        {
            this.Url = url;
            return this;
        }

        public String GetCredentials()
        {
            return Credentials.StartsWith("Basic ") ? Credentials : "Basic " + Credentials;
        }

        public override string ToString()
        {
            return Alias;
        }
    }
}