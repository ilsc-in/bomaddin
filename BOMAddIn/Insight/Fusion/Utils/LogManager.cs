using Serilog;
using Serilog.Events;

namespace Insight.Fusion.Utils
{
    public class LogManager
    {
        public static string format = "{Timestamp:HH:mm:ss} [{Level}] ({ThreadId}) {Message}{NewLine}{Exception}";
        public static ILogger log = new LoggerConfiguration().MinimumLevel.Information()
        .WriteTo.Console(outputTemplate: format, restrictedToMinimumLevel: LogEventLevel.Error)
        .WriteTo.File("log.txt", rollingInterval: RollingInterval.Day, outputTemplate: format)
        .CreateLogger();
    }
}
