﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using Serilog;
using System.Windows.Forms;
using Insight.Fusion.BOMAddIn;
using System.Collections.Generic;

namespace Insight.Fusion.Utils
{
    class XLHelper
    {
        private static ILogger log = LogManager.log;

        public static void SetCellValue(Excel.Worksheet sheet, String address, String value)
        {
            try
            {
                if (sheet.get_Range(address) is Excel.Range range)
                {
                    if (range.Count == 1)
                        range.Value = value;

                    Marshal.ReleaseComObject(range);
                    range = null;
                }
            }
            catch (Exception ex)
            {
                log.Information("Could not set value on cell {cell} in worksheet {sheet}. {message}.\n{trace}", address, sheet?.Name, ex.Message, ex.StackTrace);
                MessageBox.Show(String.Format("Could not set value on cell {0} in worksheet {1}. {2}", address, sheet?.Name, ex.Message));
            }
        }

        public static void SetCellStyle(Excel.Worksheet sheet, String addressFrom, String addressTo, String style)
        {
            try
            {
                var range = sheet.get_Range(addressFrom, addressTo) as Excel.Range;

                SetCellStyle(sheet, range, style);
            }
            catch (Exception ex)
            {
                log.Information("Could not set style on cells {from} to {to} in worksheet {sheet}. {message}.\n{trace}",
                    addressFrom, addressTo, sheet?.Name, ex.Message, ex.StackTrace);
                MessageBox.Show(String.Format("Could not set style on cells from {0} to {1} in worksheet {2}. {3}",
                    addressFrom, addressTo, sheet?.Name, ex.Message));
            }
        }

        public static void SetCellStyle(Excel.Worksheet sheet, Excel.Range range, String style)
        {
            try
            {
                if (range != null)
                {
                    range.Style = style;

                    Marshal.ReleaseComObject(range);
                    range = null;
                }
            }
            catch (Exception ex)
            {
                log.Information("Could not set style on cells {address} in worksheet {sheet}. {message}.\n{trace}",
                    range?.Address, sheet?.Name, ex.Message, ex.StackTrace);
                MessageBox.Show(String.Format("Could not set style on cells {0} in worksheet {1}. {2}",
                    range?.Address, sheet?.Name, ex.Message));
            }
        }

        public static void FindAndHighlightCells(Excel.Worksheet sheet, string from, string to, string value, string style)
        {
            Excel.Range currentFind = null;
            Excel.Range firstFind = null;
            Excel.Range range = null;

            try
            {
                //xcel.FindAll()
                range = sheet.get_Range(from, to);

                currentFind = range.Find(value);

                while (currentFind != null)
                {
                    if (firstFind == null)
                    {
                        firstFind = currentFind;
                    }
                    else if (currentFind.get_Address(Excel.XlReferenceStyle.xlA1)
                          == firstFind.get_Address(Excel.XlReferenceStyle.xlA1))
                    {
                        break;
                    }
                    currentFind.Style = style;
                    currentFind = range.FindNext(currentFind);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Failed to apply style on cells from {0} to {1} having value {2} in sheet {3}. {4}",
                    from, to, value, sheet?.Name, ex.Message));
            }
            finally
            {
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (firstFind != null)
                    Marshal.ReleaseComObject(firstFind);
                if (currentFind != null)
                    Marshal.ReleaseComObject(currentFind);
            }
        }

        public static void SetFormattingRules(Excel.Worksheet sheet, List<string> values)
        {
            Excel.FormatConditions conditions = sheet.UsedRange.FormatConditions;
            values.ForEach(value =>
            {
                Excel.FormatCondition condition = conditions.Add(Excel.XlFormatConditionType.xlCellValue, 
                    Excel.XlFormatConditionOperator.xlEqual, "=\"" + value + "\"");
                condition.Interior.ColorIndex = 3;
                condition.Font.ColorIndex = 2;
            });
        }

        public static void SetColumnWidth(Excel.Worksheet sheet, String addressFrom, String addressTo, int width)
        {
            try
            {
                if (sheet.get_Range(addressFrom, addressTo) is Excel.Range range)
                {
                    range.ColumnWidth = width;

                    Marshal.ReleaseComObject(range);
                    range = null;
                }
            }
            catch (Exception ex)
            {
                log.Information("Could not set column width for cells {from} to {to} in worksheet {sheet}. {message}.\n{trace}",
                    addressFrom, addressTo, sheet?.Name, ex.Message, ex.StackTrace);
                MessageBox.Show(String.Format("Could not set column width for cells from {0} to {1} in worksheet {2}. {3}",
                    addressFrom, addressTo, sheet?.Name, ex.Message));
            }
        }

        public static CellType GetCellType(Excel.Range cell)
        {
            CellType cellType = CellType.Text;
            if (Convert.ToBoolean(cell.HasFormula))
                cellType = CellType.Formula;
            else if (cell.Value2 == null)
                cellType = CellType.Blank;
            else if (cell.Value is double || cell.Value is decimal)
                cellType = CellType.Number;
            else if (cell.Value2 is double)
                cellType = CellType.Date;
            return cellType;
        }

        public static dynamic GetCellValue(Excel.Worksheet sheet, string address)
        {
            Excel.Range range = null;
            try
            {
                range = sheet.get_Range(address) as Excel.Range;

                if (range != null)
                {
                    if (range.Count == 1)
                    {
                        if (range.Value2 == null)
                            return null;
                        else if (range.Value is double)
                            return (double)range.Value;
                        else if (range.Value is decimal)
                            return (decimal)range.Value;
                        else if (range.Value2 is double)
                            return DateTime.FromOADate(range.Value2);
                        else
                            return range.Value2.ToString();
                    }
                    else
                    {
                        MessageBox.Show(String.Format("Address {0} must point to a single cell only.", address));
                        return null;
                    }
                }
                else
                {
                    MessageBox.Show(String.Format("Could not access cell {0} in worksheet {1}.",
                        address, sheet?.Name));
                    return null;
                }
            }
            catch (Exception ex)
            {
                log.Information("Could not get value on cell {cell} in worksheet {sheet}. {message}.\n{trace}",
                    address, sheet?.Name, ex.Message, ex.StackTrace);
                MessageBox.Show(String.Format("Could not get value on cell {0} in worksheet {1}. {2}",
                    address, sheet?.Name, ex.Message));
                return null;
            }
            finally
            {
                Marshal.ReleaseComObject(range);
                range = null;
            }
        }

        public static string GetTextValue(Excel.Worksheet sheet, string address)
        {
            return GetCellValue(sheet, address) as string;
        }

        public static DateTime? GetDateTimeValue(Excel.Worksheet sheet, string address)
        {
            return GetCellValue(sheet, address) as DateTime?;
        }

        public static double? GetDoubleValue(Excel.Worksheet sheet, string address)
        {
            return GetCellValue(sheet, address) as double?;
        }

        public static decimal? GetDecimalValue(Excel.Worksheet sheet, string address)
        {
            return GetCellValue(sheet, address) as decimal?;
        }

        public static Excel.Worksheet GetActiveWorksheet()
        {
            Excel.Worksheet sheet = null;

            try
            {
                sheet = Globals.BOMAddIn.Application.ActiveSheet as Excel.Worksheet;
            }
            catch (Exception ex)
            {
                log.Information("Could not get active worksheet. {message}.\n{trace}", ex.Message, ex.StackTrace);
                MessageBox.Show("Could not determine Active Workbook. Click anywhere on the worksheet and try again.");
                return null;
            }

            if (sheet != null) return sheet;

            MessageBox.Show("Could not determine Active Workbook. Click anywhere on the worksheet and try again.");
            return null;
        }

        public static void SetComment(Excel.Worksheet sheet, string address, string comment)
        {
            Excel.Range range = null;
            try
            {
                range = sheet.get_Range(address) as Excel.Range;
                range.AddComment(comment);
            }
            catch (Exception ex)
            {
                log.Information("Could not add comment on cell {cell} in worksheet {sheet}. {message}.\n{trace}",
                    address, sheet?.Name, ex.Message, ex.StackTrace);
                MessageBox.Show(String.Format("Could not add comment on cell {0} in worksheet {1}. {2}",
                    address, sheet?.Name, ex.Message));
            }
            finally
            {
                Marshal.ReleaseComObject(range);
                range = null;
            }
        }

        public static void ClearComment(Excel.Worksheet sheet, string address)
        {
            Excel.Range range = null;
            try
            {
                range = sheet.get_Range(address) as Excel.Range;
                range.ClearComments();
            }
            catch (Exception ex)
            {
                log.Information("Could not clear comments on cell {cell} in worksheet {sheet}. {message}.\n{trace}",
                    address, sheet?.Name, ex.Message, ex.StackTrace);
                MessageBox.Show(String.Format("Could not clear comments on cell {0} in worksheet {1}. {2}",
                    address, sheet?.Name, ex.Message));
            }
            finally
            {
                Marshal.ReleaseComObject(range);
                range = null;
            }
        }

        public enum CellType
        {
            Blank,
            Date,
            Formula,
            Number,
            Text
        }
    }
}
