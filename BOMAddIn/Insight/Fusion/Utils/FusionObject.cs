using System;
using System.Linq;
using System.Collections.Generic;

namespace Insight.Fusion.Utils
{
    public class FusionObject
    {
        public Dictionary<String, String> Attributes { get; set; }
        public FusionObject()
        {
            Attributes = new Dictionary<string, string>();
        }
        public FusionObject Set(String attr, String value)
        {
            if (attr == null) throw new ArgumentNullException("AttributeName");

            if (Attributes.ContainsKey(attr)) Attributes[attr] = value;
            else Attributes.Add(attr, value);

            return this;
        }

        public override string ToString()
        {
            return String.Join(", ", Attributes?.Select(e => e.Key + "=" + e.Value) ?? null);
        }
    }
}