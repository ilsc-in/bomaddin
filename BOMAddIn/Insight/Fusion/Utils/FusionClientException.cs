using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Insight.Fusion.Utils
{
    public class FusionClientException : Exception
    {
        public APIResponse Response { get; set; }
        public FusionClientException(String message, Exception ex) : base(message, ex)
        {
        }
        public FusionClientException(String message) : base(message)
        {
        }
        public FusionClientException(APIResponse response, String message, Exception ex)
        : this(message += response.Document?.GetErrors() ?? "", ex)
        {
            this.Response = response;
        }
        public FusionClientException(APIResponse response, String message)
        : this(message += response.Document?.GetErrors() ?? "")
        {
            this.Response = response;
        }

    }
}