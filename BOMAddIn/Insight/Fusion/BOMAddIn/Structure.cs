using Insight.Fusion.Utils;
using System;
using System.Collections.Generic;

namespace Insight.Fusion.BOMAddIn
{
    public class Structure : FusionObject
    {
        public bool Exists { get; set; }
        public bool BadPType { get; set; }

        public string ParentItem => Attributes?.GetValueOrDefault("ItemNumber", "INVALID_ITEM");
        public string OrgCode { get { return Attributes?.GetValueOrDefault("OrganizationCode", "INVALID_ORG"); } }
        public string StructureName { get { return Attributes?.GetValueOrDefault("StructureName", "Primary"); } }
        public string EffControlValue { get { return Attributes?.GetValueOrDefault("EffectivityControlValue", "Date"); } }

        public FusionObject StructureDFF { get; set; }
        public List<Component> Components { get; private set; }

        public Structure(string number, string orgCode, bool multiPType = false, List<Component> components = null,
            FusionObject structureDFF = null, string name = "Primary", string effControl = "Date")
        {
            if (String.IsNullOrWhiteSpace(number) || String.IsNullOrWhiteSpace(orgCode))
                throw new ArgumentException("Parent Item Number and Organization Code can't be null or blank");

            Attributes = new Dictionary<string, string>(){
                {"ItemNumber", number},
                {"OrganizationCode", orgCode},
                {"StructureName", StructureName},
                {"EffectivityControlValue", EffControlValue}
            };

            this.Components = components ?? new List<Component>();
            this.StructureDFF = structureDFF ?? new FusionObject();
        }

        public Structure Add(Component cmp)
        {
            if (this.Components.Find(c => c.ItemNumber == cmp.ItemNumber) != null)
                throw new System.ArgumentException("Component {0} already exists in structure");

            this.Components.Add(cmp);

            return this;
        }
        public Structure SetDFF(String attr, String value)
        {
            StructureDFF.Set(attr, value);
            return this;
        }
        public new Structure Set(String attr, String value)
        {
            base.Set(attr, value);
            return this;
        }
    }
    public class Component : FusionObject
    {
        public string ItemNumber { get => Attributes.GetValueOrDefault("ComponentItemNumber", "INVALID_ITEM"); }
        public int Quantity { get => Int32.Parse(Attributes.GetValueOrDefault("Quantity", "0")); }
        public int Yield { get => Int32.Parse(Attributes.GetValueOrDefault("Yield", "1")); }
        public string Basis { get => Attributes.GetValueOrDefault("BasisValue", "Variable"); }
        public Component(string number, int qty = 0, float yield = 1, string basis = "Variable")
        {
            if (String.IsNullOrWhiteSpace(number))
                throw new ArgumentException("Component Item Number Code can't be null or blank");

            Attributes = new Dictionary<string, string>(){
                {"ComponentItemNumber", number},
                {"Quantity", qty.ToString()},
                {"Yield", yield.ToString()},
                {"BasisValue", basis}
            };
        }
        public Component(string number, string qty = "0", string yield = "1", string basis = "Variable")
        {
            if (String.IsNullOrWhiteSpace(number))
                throw new ArgumentException("Component Item Number Code can't be null or blank");

            Attributes = new Dictionary<string, string>(){
                {"ComponentItemNumber", number},
                {"Quantity", qty ?? "0"},
                {"Yield", yield ?? "1"},
                {"BasisValue", basis}
            };
        }
        public new Component Set(String attr, String value)
        {
            base.Set(attr, value);
            return this;
        }
    }
}