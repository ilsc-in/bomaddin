﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Insight.Fusion.BOMAddIn
{
    public class ProcessType
    {
        public String Code { get; set; }

        public String Pattern { get; set; }

        public int Sequence { get; set; }

        public static List<String> Resolve(String number, List<ProcessType> processTypes)
        {
            var types = processTypes.FindAll(type => Regex.Match(number, type.Pattern).Success)
                .Select(type => type.Code).ToList();

            return types?.Count > 0 ? types : null;
        }
    }
}
