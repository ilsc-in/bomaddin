﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Fusion.BOMAddIn
{
    public class TemplateMap
    {
        public string OrgCode { get; set; }
        public string Product { get; set; }
        public string MaterialType { get; set; }
        public string ItemTemplate { get; set; }
    }
}
