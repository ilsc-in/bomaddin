using System;
using System.Net;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Serilog;
using Insight.Fusion.Utils;
using RestSharp;

namespace Insight.Fusion.BOMAddIn
{
    public class BOMClient : FusionClient
    {
        private static ILogger Log = LogManager.log;

        public static readonly string ITM_ENDPOINT = "fscmService/ItemServiceV2";
        public static readonly string STR_ENDPOINT = "fscmService/StructureServiceV2";
        public static readonly string WD_ENDPOINT = "fscmRestApi/resources/11.13.18.05/workDefinitionRequests";

        public static readonly string FIND_ITM_SOAP_ACTION = @"http://xmlns.oracle.com/apps/scm/productModel/items/itemServiceV2/findItem";
        public static readonly string PROCESS_ITM_SOAP_ACTION = @"http://xmlns.oracle.com/apps/scm/productModel/items/itemServiceV2/processItem";
        public static readonly string FIND_STR_SOAP_ACTION = @"http://xmlns.oracle.com/apps/scm/productModel/items/structures/structureServiceV2/findStructure";
        public static readonly string CRE_STR_SOAP_ACTION = @"http://xmlns.oracle.com/apps/scm/productModel/items/structures/structureServiceV2/createStructure";

        public static readonly XNamespace soapNS = @"http://schemas.xmlsoap.org/soap/envelope/";
        public static readonly XNamespace itmNS = @"http://xmlns.oracle.com/apps/scm/productModel/items/itemServiceV2/";
        public static readonly XNamespace itmTypNS = @"http://xmlns.oracle.com/apps/scm/productModel/items/itemServiceV2/types/";
        public static readonly XNamespace strNS = @"http://xmlns.oracle.com/apps/scm/productModel/items/structures/structureServiceV2/";
        public static readonly XNamespace strTypNS = @"http://xmlns.oracle.com/apps/scm/productModel/items/structures/structureServiceV2/types/";
        public static readonly XNamespace strDffNS = @"http://xmlns.oracle.com/apps/scm/productModel/items/structures/flex/structureHeader/";
        public static readonly XNamespace adfTypNS = @"http://xmlns.oracle.com/adf/svc/types/";

        public List<Item> CheckIfItemsExistInOrg(String orgCode, List<Item> items, int maxBatchSize = 500)
        {
            if (items?.Count < 1) throw new ArgumentException("Item List can't be null or empty");

            Log.Information("Checking if items exist in org {orgCode}", orgCode);
            int index = 0;

            Log.Information("Total item count {count}, will be processed in {batchCount}",
                items.Count, (items.Count > maxBatchSize ? ((((int)(items.Count / maxBatchSize) + 1)) + " batches") : "1 batch"));
            do
            {
                int size = items.Count > (index + maxBatchSize) ? maxBatchSize : items.Count - index;

                Log.Information("Processing items from {start} to {end}", index + 1, index + size);
                List<Item> itemBatch = items.GetRange(index, size);

                var payload = BuildFindItemReq(orgCode, itemBatch.Select(i => i.ItemNumber).ToList()).ToString();
                Log.Debug("Find Item payload:\n{payload}", payload);

                var apiResponse = GetResponse(BuildRequest(ITM_ENDPOINT, FIND_ITM_SOAP_ACTION, payload));
                Log.Debug("API Response:\n{apiResponse}", apiResponse.ToString());

                if (apiResponse.Status == HttpStatusCode.OK)
                {
                    if (apiResponse.Document != null)
                    {
                        List<String> itemsFound = apiResponse.Document
                        .Descendants()
                        .Where(elm =>
                        {
                            return elm.Name.Equals(adfTypNS + "Value");
                        })
                        .Select(elm => elm.Element(itmNS + "ItemNumber").Value)
                        .ToList();

                        foreach (Item item in itemBatch)
                            item.Exists = itemsFound.Contains(item.ItemNumber);
                    }
                }
                else
                {
                    throw new FusionClientException(apiResponse, "Failed to execute find request");
                }
                index += maxBatchSize;
            }
            while (index < items.Count);

            Log.Information("Missing item count is {count}", items.Count(i => !i.Exists));

            return items;
        }

        public List<Structure> CheckIfStructuresExistInOrg(String orgCode, List<Structure> structures, int maxBatchSize = 500, String structureName = "Primary")
        {
            if (structures?.Count < 1) throw new ArgumentException("Structure List can't be null or empty");

            Log.Information("Checking if structures exist in org {orgCode}", orgCode);
            int index = 0;

            Log.Information("Total structure count {count}, will be processed in {batchCount}",
                structures.Count, (structures.Count > maxBatchSize ? ((((int)(structures.Count / maxBatchSize) + 1)) + " batches") : "1 batch"));
            do
            {
                int size = structures.Count > (index + maxBatchSize) ? maxBatchSize : structures.Count - index;

                Log.Information("Processing structures from {start} to {end}", index + 1, index + size);
                List<Structure> structureBatch = structures.GetRange(index, size);

                var payload = BuildFindStructureReq(orgCode, structureName, structureBatch.Select(s => s.ParentItem).ToList()).ToString();
                Log.Debug("Find structure payload:\n{payload}", payload);

                var apiResponse = GetResponse(BuildRequest(STR_ENDPOINT, FIND_STR_SOAP_ACTION, payload));
                Log.Debug("API Response:\n{apiResponse}", apiResponse.ToString());

                if (apiResponse.Status == HttpStatusCode.OK)
                {
                    if (apiResponse.Document != null)
                    {
                        List<String> structuresFound = apiResponse.Document
                        .Descendants()
                        .Where(elm =>
                        {
                            return elm.Name.Equals(strNS + "Value");
                        })
                        .Select(elm => elm.Element(strNS + "ItemNumber").Value)
                        .ToList();

                        foreach (Structure structure in structureBatch)
                            structure.Exists = structuresFound.Contains(structure.ParentItem);
                    }
                }
                else
                {
                    throw new FusionClientException(apiResponse, "Failed to execute find request");
                }
                index += maxBatchSize;
            }
            while (index < structures.Count);

            Log.Information("Missing structure count is {count}", structures.Count(i => !i.Exists));

            return structures;
        }

        public List<Item> CreateMissingItems(List<Item> items, int maxBatchSize = 100)
        {
            if (items?.Count < 1) throw new ArgumentException("Item List can't be null or empty");

            Log.Information("Creating missing items");
            var missingItems = items.Where(i => !i.Exists).ToList();

            if (missingItems.Count > 0)
            {
                Tracker.Init("Creating items in Org " + items.First().OrgCode, items.Count);
                Tracker.SetProgress(items.Count(i => i.Exists), items.Count);
                var itemGroups = missingItems.ChunkBy(maxBatchSize);
                Log.Information("Total missing item count is {count}, it will be processed in {batchCount}",
                    missingItems.Count, itemGroups.Count > 1 ? itemGroups.Count + " batches" : " a single batch");

                var exceptions = new ConcurrentQueue<Exception>();
                Parallel.ForEach(itemGroups, (List<Item> itemBatch, ParallelLoopState state) =>
                {
                    Log.Information("Processing items from {start} to {end}", missingItems.IndexOf(itemBatch.First()),
                        missingItems.IndexOf(itemBatch.Last()));

                    var payload = BuildProcessItemReq(itemBatch).ToString();
                    Log.Debug("Process Item payload:\n{payload}", payload);

                    var apiResponse = GetResponse(BuildRequest(ITM_ENDPOINT, FIND_ITM_SOAP_ACTION, payload));
                    Log.Debug("API Response:\n{apiResponse}", apiResponse.ToString());

                    if (apiResponse.Status == HttpStatusCode.OK)
                    {
                        itemBatch.ForEach(i => i.Exists = true);
                        Tracker.SetProgress(items.Count(i => i.Exists), items.Count);
                    }
                    else
                    {
                        apiResponse.Payload = payload;
                        Log.Error("Failed to execute create item request. API Response:\n{apiResponse}", apiResponse);
                        exceptions.Enqueue(new FusionClientException(apiResponse, "Failed to execute create structure request"));
                        state.Stop();
                    }
                });

                if (exceptions.Count > 0) throw new AggregateException(exceptions);
            }

            Log.Information("Missing item count is {count}", items.Count(i => !i.Exists));

            return items;
        }

        public List<Item> CreateWorkDefinitions(String orgCode, String product, List<Item> items)
        {
            if (items?.Count < 1) throw new ArgumentException("Item List can't be null or empty");

            Log.Information("Creating Work Definitions for {count} items in org {orgCode}", items.Count, orgCode);
            Tracker.Init("Creating work definitions in Org " + items.First().OrgCode, items.Count);
            Tracker.SetProgress(Convert.ToInt32(items.Count * 0.1), items.Count);

            IRestRequest request = BuildCreateWDReq(orgCode, product, items);
            var payload = request.Parameters.Find(p => p.Type == ParameterType.RequestBody)?.Value?.ToString();
            Log.Debug("Create Work Definition payload:\n{payload}", payload);

            var apiResponse = GetResponse(request);
            Log.Debug("API Response:\n{apiResponse}", apiResponse.ToString());

            if (apiResponse.Status == HttpStatusCode.Created)
            {
                items.ForEach(i => i.WDExists = true);

                var wdResp = WorkDefinitionResponse.FromJson(apiResponse.Text);
                if (wdResp.ErrorsExistFlag.HasValue && wdResp.ErrorsExistFlag.Value)
                {
                    var failed = wdResp.WorkDefinitionHeaders.FindAll(wdH => !String.IsNullOrWhiteSpace(wdH.ErrorMessageNames)
                           && wdH.ErrorMessageNames != "WIS_WD_VER_NUM_DUPL");

                    if (failed?.Count > 0)
                    {
                        failed.ForEach(wdH => items.Find(i => i.ItemNumber == wdH.ItemNumber).WDExists = false);
                        apiResponse.Payload = payload;
                        Log.Error("Work Definition creation failed for {failCnt} out of {total} items. Review errors in response.\n{response}",
                            failed.Count, items.Count, apiResponse.ToString());
                    }
                }
                Tracker.SetProgress(items.Count, items.Count);
                Tracker.OnCompletion();
                return items;
            }
            else
            {
                apiResponse.Payload = payload;
                Log.Information("Creating Work Definitions");
                throw new FusionClientException(apiResponse, "Failed to execute create Work Definition request");
            }
        }

        public List<Structure> CreateMissingStructures(List<Structure> structures)
        {
            if (structures?.Count < 1) throw new ArgumentException("Structure List can't be null or empty");

            Log.Information("Creating missing structures");
            List<Structure> missingStructures = structures.FindAll(s => !s.Exists);
            Log.Information("Missing structure count is {count}", missingStructures.Count);
            if (missingStructures.Count > 0)
            {
                Tracker.Init("Creating Structures", structures.Count);
                Tracker.SetProgress(0, structures.Count);
                Tracker.SetProgress(structures.Count(i => i.Exists), structures.Count);

                var exceptions = new ConcurrentQueue<Exception>();

                Parallel.ForEach(missingStructures, (Structure structure, ParallelLoopState state) =>
                {
                    if (structure == null)
                    {
                        exceptions.Enqueue(new ArgumentNullException("Structure"));
                        state.Stop();
                    }

                    Log.Information("Creating structure for parent {parent} with {comps} component line(s)",
                        structure.ParentItem, structure.Components.Count);

                    String payload = BuildCreateStructureReq(structure).ToString();
                    Log.Debug("Create Structure payload:\n{payload}", payload);

                    var apiResponse = GetResponse(BuildRequest(STR_ENDPOINT, CRE_STR_SOAP_ACTION, payload));
                    Log.Debug("API Response:\n{apiResponse}", apiResponse.ToString());

                    if (apiResponse.Status == HttpStatusCode.OK)
                    {
                        structure.Exists = true;
                        Tracker.SetProgress(structures.Count(i => i.Exists), structures.Count);
                    }
                    else
                    {
                        apiResponse.Payload = payload;
                        Log.Error("Failed to execute create structure request. API Response:\n{apiResponse}", apiResponse);
                        exceptions.Enqueue(new FusionClientException(apiResponse, "Failed to execute create structure request"));

                        state.Stop();
                    }
                });

                if (exceptions.Count > 0) throw new AggregateException(exceptions);

                Tracker.SetProgress(structures.Count(i => i.Exists), structures.Count);
                Log.Information("Missing structure count is {count}", structures.Count(i => !i.Exists));

                Tracker.OnCompletion();
            }
            
            return structures;
        }

        private static XElement BuildFindItemReq(string orgCode, List<String> items, int start = 0, int size = -1)
        {
            return new XElement(soapNS + "Envelope",
                new XElement(soapNS + "Body",
                  new XElement(itmTypNS + "findItem",
                    new XElement(itmTypNS + "findCriteria",
                        new XElement(adfTypNS + "fetchStart", start),
                        new XElement(adfTypNS + "fetchSize", size),
                        new XElement(adfTypNS + "filter",
                            new XElement(adfTypNS + "group",
                                new XElement(adfTypNS + "item",
                                    new XElement(adfTypNS + "conjunction", "Or"),
                                    new XElement(adfTypNS + "attribute", "ItemNumber"),
                                    new XElement(adfTypNS + "operator", "="),
                                    new XElement(adfTypNS + "value", items.First())
                                ),
                                from item in items.Skip(1)
                                select new XElement(adfTypNS + "item",
                                    new XElement(adfTypNS + "attribute", "ItemNumber"),
                                    new XElement(adfTypNS + "operator", "="),
                                    new XElement(adfTypNS + "value", item)
                                )
                            ),
                            new XElement(adfTypNS + "group",
                                new XElement(adfTypNS + "conjunction", "And"),
                                new XElement(adfTypNS + "item",
                                    new XElement(adfTypNS + "attribute", "OrganizationCode"),
                                    new XElement(adfTypNS + "operator", "="),
                                    new XElement(adfTypNS + "value", orgCode)
                                )
                            )
                        ),
                        new XElement(adfTypNS + "findAttribute", "ItemNumber")
                    )
                  )
                )
            );
        }

        private static XElement BuildFindStructureReq(string orgCode, string structureName, List<String> items, int start = 0, int size = -1)
        {
            return new XElement(soapNS + "Envelope",
                new XElement(soapNS + "Body",
                  new XElement(strTypNS + "findStructure",
                    new XElement(strTypNS + "findCriteria",
                        new XElement(adfTypNS + "fetchStart", start),
                        new XElement(adfTypNS + "fetchSize", size),
                        new XElement(adfTypNS + "filter",
                            new XElement(adfTypNS + "group",
                                new XElement(adfTypNS + "item",
                                    new XElement(adfTypNS + "conjunction", "Or"),
                                    new XElement(adfTypNS + "attribute", "ItemNumber"),
                                    new XElement(adfTypNS + "operator", "="),
                                    new XElement(adfTypNS + "value", items.First())
                                ),
                                from item in items.Skip(1)
                                select new XElement(adfTypNS + "item",
                                    new XElement(adfTypNS + "attribute", "ItemNumber"),
                                    new XElement(adfTypNS + "operator", "="),
                                    new XElement(adfTypNS + "value", item)
                                )
                            ),
                            new XElement(adfTypNS + "group",
                                new XElement(adfTypNS + "conjunction", "And"),
                                new XElement(adfTypNS + "item",
                                    new XElement(adfTypNS + "attribute", "OrganizationCode"),
                                    new XElement(adfTypNS + "operator", "="),
                                    new XElement(adfTypNS + "value", orgCode)
                                )
                            ),
                            new XElement(adfTypNS + "group",
                                new XElement(adfTypNS + "conjunction", "And"),
                                new XElement(adfTypNS + "item",
                                    new XElement(adfTypNS + "attribute", "StructureName"),
                                    new XElement(adfTypNS + "operator", "="),
                                    new XElement(adfTypNS + "value", structureName)
                                )
                            )
                        ),
                        new XElement(adfTypNS + "findAttribute", "ItemNumber")
                    )
                  )
                )
            );
        }

        private static XElement BuildProcessItemReq(List<Item> items, string operation = "Create", bool partialFailureAllowed = false)
        {
            return new XElement(soapNS + "Envelope",
               new XAttribute(XNamespace.Xmlns + "ns0", itmTypNS.NamespaceName),
               new XAttribute(XNamespace.Xmlns + "ns1", itmNS.NamespaceName),
               new XAttribute(XNamespace.Xmlns + "ns2", adfTypNS.NamespaceName),
               new XElement(soapNS + "Body",
                  new XElement(itmTypNS + "processItem",
                     new XElement(itmTypNS + "changeOperation", operation),
                     from item in items
                     select new XElement(itmTypNS + "item",
                        from atr in item.Attributes
                        select new XElement(itmNS + atr.Key, atr.Value)
                     ),
                     new XElement(itmTypNS + "processControl",
                        new XElement(adfTypNS + "returnMode", "Key"),
                        new XElement(adfTypNS + "partialFailureAllowed", partialFailureAllowed)
                     )
                  )
               )
            );
        }

        private static XElement BuildCreateStructureReq(Structure structure)
        {
            return new XElement(soapNS + "Envelope",
               new XAttribute(XNamespace.Xmlns + "ns0", strNS.NamespaceName),
               new XAttribute(XNamespace.Xmlns + "ns1", strTypNS.NamespaceName),
               new XAttribute(XNamespace.Xmlns + "ns2", strDffNS.NamespaceName),
               new XElement(soapNS + "Body",
                  new XElement(strTypNS + "createStructure",
                     new XElement(strTypNS + "structure",
                        from atr in structure.Attributes
                        select new XElement(strNS + atr.Key, atr.Value),

                        from cmp in structure.Components
                        select new XElement(strNS + "Component",
                            from atrCmp in cmp.Attributes
                            select new XElement(strNS + atrCmp.Key, atrCmp.Value)
                        ),

                        new XElement(strNS + "StructureDFF",
                            from atrDff in structure.StructureDFF.Attributes
                            select new XElement(strDffNS + atrDff.Key, atrDff.Value)
                        )
                     )
                  )
               )
            );
        }

        private static IRestRequest BuildCreateWDReq(string orgCode, string product, List<Item> items)
        {
            var wd = new WorkDefinitionRequest
            {
                WorkDefinitionHeaders = items.Select(item => new WorkDefinitionHeader
                {
                    ActionCode = "CREATE",
                    ItemNumber = item.ItemNumber,
                    OrganizationCode = orgCode,
                    VersionNumber = 1,
                    WorkMethodCode = "DISCRETE_MANUFACTURING",
                    WorkDefinitionCode = "ORA_MAIN",
                    ItemStructureName = "Primary",
                    CompletionSubinventoryCode = product + "-FG",
                    ProductionPriority = 1,
                    CostingPriority = 1,
                    Operations = new List<Operation>() { new Operation{
                    ActionCode="CREATE",
                    OperationSequenceNumber= 10,
                    StandardOperationCode= "IKT COMMON STD OP",
                    OperationType= "IN_HOUSE"
                } }
                }).ToList()
            };

            var request = new RestRequest(WD_ENDPOINT, Method.POST);
            request.AddParameter("application/json", wd.ToJson(), ParameterType.RequestBody);
            return request;
        }
    }
}