﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Insight.Fusion.Utils;

namespace Insight.Fusion.BOMAddIn
{
    public class Config
    {
        public String DefaultItemClass { get; set; }
        public String DefaultItemTemplate { get; set; }
        public String ItemMasterOrg { get; set; }
        public List<ProcessType> ProcessTypes { get; set; }
        public List<TemplateMap> templates { get; set; }

        List<FusionEnv> environments = new List<FusionEnv>();

        public static Config Load(String cfgFile = null)
        {
            cfgFile = cfgFile ?? Environment.GetEnvironmentVariable("BOM_CFG_FILE") ?? Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\bom_config.xml";

            Config config = new Config();

            if (new FileInfo(cfgFile).Exists)
            {
                XElement cfgDoc = XElement.Load(cfgFile);

                config.DefaultItemClass = cfgDoc.Element("DefaultItemClass")?.Value;
                config.DefaultItemTemplate = cfgDoc.Element("DefaultItemTemplate")?.Value;
                config.ItemMasterOrg = cfgDoc.Element("ItemMasterOrg")?.Value;

                if (String.IsNullOrWhiteSpace(config.DefaultItemClass))
                    throw new Exception(String.Format("Default Item Class definition is missing in config file {0}", cfgFile));

                if (String.IsNullOrWhiteSpace(config.DefaultItemTemplate))
                    throw new Exception(String.Format("Default Item Class definition is missing in config file {0}", cfgFile));

                if (String.IsNullOrWhiteSpace(config.ItemMasterOrg))
                    throw new Exception(String.Format("Default Item Class definition is missing in config file {0}", cfgFile));

                config.ProcessTypes = cfgDoc.Element("ProcessTypes")?.Elements("ProcessType")?.Select(pt =>
                    new ProcessType { Code = pt.Element("Code").Value, Pattern = pt.Element("Pattern").Value }
                ).ToList();

                if (config.ProcessTypes == null || config.ProcessTypes.Count < 1)
                    throw new Exception(String.Format("Process Type definitions are missing in config file {0}", cfgFile));

                config.templates = cfgDoc.Element("TemplateMaps")?.Elements("TemplateMap")?.Select(tm =>
                    new TemplateMap { OrgCode = tm.Element("OrgCode").Value, MaterialType = tm.Element("MaterialType").Value,
                        Product = tm.Element("Product").Value, ItemTemplate = tm.Element("ItemTemplate").Value}
                ).ToList();

                if (config.templates == null || config.templates.Count < 1)
                    throw new Exception(String.Format("Item Template mapping to material types are missing in config file {0}", cfgFile));

                config.environments = FusionEnv.Load();

                return config;
            }
            else
            {
                throw new Exception(String.Format("Configuration file {0} is missing.", cfgFile));
            }
        }

        public static void SaveToFile(Config config)
        {
            String cfgFile = Environment.GetEnvironmentVariable("BOM_CFG_FILE") ?? Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\bom_config.xml";

            String xml = new XElement("Config",
                new XElement("DefaultItemClass", config.DefaultItemClass),
                new XElement("ItemMasterOrg", config.ItemMasterOrg),
                new XElement("DefaultItemTemplate", config.DefaultItemTemplate),
                new XElement("TemplateMaps",
                from template in config.templates
                select new XElement("TemplateMap",
                    new XElement("OrgCode", template.OrgCode),
                    new XElement("Product", template.Product),
                    new XElement("MaterialType", template.MaterialType),
                    new XElement("ItemTemplate", template.ItemTemplate)
                )),
                new XElement("ProcessTypes",
                from pt in config.ProcessTypes
                select new XElement("ProcessType",
                    new XElement("Code", pt.Code),
                    new XElement("Pattern", pt.Pattern)
                ))
            ).ToString();

            WriteToFile(cfgFile, xml);
        }

        public static void WriteToFile(String path, String text)
        {
            FileInfo fi = new FileInfo(path);
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.Write(text);
            }
        }
    }
}
