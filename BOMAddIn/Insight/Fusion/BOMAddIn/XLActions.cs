﻿using System;
using System.Collections.Generic;
using Excel = Microsoft.Office.Interop.Excel;
using XL = Insight.Fusion.Utils.XLHelper;
using System.Runtime.InteropServices;
using Serilog;
using Insight.Fusion.Utils;
using System.Windows.Forms;

namespace Insight.Fusion.BOMAddIn
{
    public class XLActions
    {
        private static ILogger log = LogManager.log;

        public static void SetupLoadSheet()
        {
            Excel.Workbook wb = null;
            Excel.Worksheet newSheet = null;

            try
            {
                wb = Globals.BOMAddIn.Application.ActiveWorkbook;
                newSheet = wb.Worksheets.Add() as Excel.Worksheet;

                if (newSheet != null)
                {
                    try
                    {
                        XL.SetCellValue(newSheet, "A1", "Organization");
                        XL.SetCellValue(newSheet, "B1", "IKT");
                        XL.SetCellValue(newSheet, "A2", "Product");
                        XL.SetCellValue(newSheet, "B2", "ITS");
                        XL.SetCellValue(newSheet, "A3", "Structure");
                        XL.SetCellValue(newSheet, "B3", "Primary");
                        XL.SetCellValue(newSheet, "A4", "Effectivity Control");
                        XL.SetCellValue(newSheet, "B4", "Date");

                        XL.SetCellStyle(newSheet, "A1", "A4", "Accent1");

                        XL.SetCellValue(newSheet, "A6", "Parent Item");
                        XL.SetCellValue(newSheet, "B6", "Parent Description");
                        XL.SetCellValue(newSheet, "C6", "Parent UOM");
                        XL.SetCellValue(newSheet, "D6", "Material Type");
                        XL.SetCellValue(newSheet, "E6", "Process Type");

                        XL.SetCellValue(newSheet, "F6", "Child Item");
                        XL.SetCellValue(newSheet, "G6", "Child Description");
                        XL.SetCellValue(newSheet, "H6", "Child UOM");
                        XL.SetCellValue(newSheet, "I6", "Material Type");

                        XL.SetCellValue(newSheet, "J6", "Quantity");
                        XL.SetCellValue(newSheet, "K6", "Basis");
                        XL.SetCellValue(newSheet, "L6", "Yield");

                        XL.SetCellStyle(newSheet, "A6", "L6", "Accent1");

                        XL.SetColumnWidth(newSheet, "A6", "B6", 20);
                        XL.SetColumnWidth(newSheet, "C6", "L6", 15);
                    }
                    catch (Exception ex)
                    {
                        log.Information("Error occurred while preparing upload template.", ex.Message, ex.StackTrace);
                        MessageBox.Show("Error occurred while preparing upload template.", ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Click on determine Activeworkbook. Click on the worksheet and try again.");
                }

            }
            catch (Exception ex)
            {
                log.Information("Could not get active worksheet. {message}.\n{trace}", ex.Message, ex.StackTrace);
                MessageBox.Show("Could not determine Active Workbook. Click anywhere on the worksheet and try again.");
            }
            finally
            {
                if (newSheet != null)
                    Marshal.ReleaseComObject(newSheet);
                if (wb != null)
                    Marshal.ReleaseComObject(wb);
            }
        }

        public static void ResetFormats()
        {
            Excel.Worksheet bomSheet = XL.GetActiveWorksheet(); ;
            if (bomSheet != null)
            {
                try
                {
                    int cnt = bomSheet.UsedRange.Rows.Count;
                    if (cnt < 7) cnt = 7;
                    XL.SetCellStyle(bomSheet, "A7", "L" + cnt, "Normal");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Failed to reset format on cells in worksheet {0}. {1}", bomSheet?.Name, ex.Message));
                }
                finally
                {
                    if (bomSheet != null)
                        Marshal.ReleaseComObject(bomSheet);
                }
            }
        }

        public static void SetStyle(string from, string to, string style = "Normal")
        {
            Excel.Worksheet bomSheet = XL.GetActiveWorksheet(); ;
            if (bomSheet != null)
            {
                try
                {
                    XL.SetCellStyle(bomSheet, from, to, style);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Failed to set format on cells in worksheet {0}. {1}", bomSheet?.Name, ex.Message));
                }
                finally
                {
                    if (bomSheet != null)
                        Marshal.ReleaseComObject(bomSheet);
                }
            }
        }

        public static void ReadDataFromSheet(FusionRibbon ribbon)
        {
            Excel.Worksheet bomSheet = null;
            try
            {
                ribbon.Reset();

                bomSheet = XL.GetActiveWorksheet();
                if (bomSheet == null) return;


                ribbon.OrgCode = XL.GetTextValue(bomSheet, "B1");
                if (String.IsNullOrWhiteSpace(ribbon.OrgCode))
                    throw new Exception("Organization code can't be left blank.");

                ribbon.Product = XL.GetTextValue(bomSheet, "B2");
                if (String.IsNullOrWhiteSpace(ribbon.Product))
                    throw new Exception("Product can't be left blank.");

                ribbon.StrName = XL.GetTextValue(bomSheet, "B3");
                if (String.IsNullOrWhiteSpace(ribbon.StrName) || (ribbon.StrName != "Primary" && ribbon.StrName != "Alternate"))
                    throw new Exception("Structure name must be set to either Primary or Alternate.");

                ribbon.EffType = XL.GetTextValue(bomSheet, "B4");
                if (String.IsNullOrWhiteSpace(ribbon.EffType))
                    throw new Exception("Effectivity Control value can't be left blank.");

                int row = 7;
                while (XL.GetCellValue(bomSheet, "A" + row) != null)
                {
                    AddToItems(bomSheet, row, 'A', ribbon);
                    AddToItems(bomSheet, row, 'F', ribbon);

                    AddToStructures(bomSheet, row, ribbon);

                    row++;
                }
                if (ribbon.Items.Count > 0 && ribbon.Structures.Count > 0)
                    ribbon.DataRead = true;
                else
                    throw new Exception("Atleast one valid structure information must be available in the current worksheet.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Failed to read data from worksheet {0}. {1}", bomSheet?.Name, ex.Message));
                ribbon.Reset();
            }
            finally
            {
                if (bomSheet != null)
                    Marshal.ReleaseComObject(bomSheet);
            }
        }

        public static void UpdateProcessTypes(FusionRibbon ribbon)
        {
            Excel.Worksheet bomSheet = null;
            try
            {
                bomSheet = XL.GetActiveWorksheet();
                if (bomSheet == null) return;
                                
                int row = 7;
                string number = XL.GetTextValue(bomSheet, "A" + row);
                while (number != null)
                {
                    string pType = XL.GetTextValue(bomSheet, "E" + row);
                    ribbon?.Structures?.Find(s => s.ParentItem == number).SetDFF("processType", pType);
                    row++;
                    number = XL.GetTextValue(bomSheet, "A" + row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Failed to read data from worksheet {0}. {1}", bomSheet?.Name, ex.Message));
                ribbon.Reset();
            }
            finally
            {
                if (bomSheet != null)
                    Marshal.ReleaseComObject(bomSheet);
            }
        }

        public static void FindAndHighlightCells(string from, string to, List<string> values, string style)
        {
            Excel.Worksheet bomSheet = XL.GetActiveWorksheet(); ;
            if (bomSheet != null)
            {
                try
                {
                    values.ForEach(value => XL.FindAndHighlightCells(bomSheet, from, to, value, style));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Failed to find and highlight cells in worksheet {0}. {1}", bomSheet?.Name, ex.Message));
                }
                finally
                {
                    if (bomSheet != null)
                        Marshal.ReleaseComObject(bomSheet);
                }
            }
        }

        public static void SetFormattingRules(List<string> values)
        {
            Excel.Worksheet bomSheet = XL.GetActiveWorksheet(); ;
            if (bomSheet != null)
            {
                XL.SetFormattingRules(bomSheet, values);
            }
        }

        private static void AddToStructures(Excel.Worksheet sheet, int row,
                    FusionRibbon ribbon)
        {
            string parent = XL.GetTextValue(sheet, "A" + row);
            string childItem = XL.GetTextValue(sheet, "F" + row);

            List<string> processType = ProcessType.Resolve(parent, ribbon.Config.ProcessTypes);

            bool badPType = true;
            string pTypeValue = null;

            XL.ClearComment(sheet, "E" + row);

            if (processType?.Count > 1)
            {
                pTypeValue = String.Join(",", processType);
                XL.SetComment(sheet, "E" + row, String.Join(", ", processType));
            }
            else if (processType?.Count == 1)
            {
                badPType = false;
                pTypeValue = processType[0];
            }

            XL.SetCellValue(sheet, "E" + row, pTypeValue);
            if (badPType)
            {
                XL.SetCellStyle(sheet, "E" + row, "E" + row, "Bad");
            }

            Structure structure = ribbon.Structures.Find(i => i.ParentItem == parent);
            if (structure == null)
            {
                structure = new Structure(parent, ribbon.OrgCode, badPType, new List<Component>(),
                    new FusionObject().Set("processType", pTypeValue), ribbon.StrName, ribbon.EffType);
                ribbon.Structures.Add(structure);
            }

            if (structure.Components.Find(c => c.ItemNumber == null) != null)
                throw new Exception(String.Format("Component {0} is appearing twice in structure of item {1}.", childItem, parent));

            string qty = XL.GetCellValue(sheet, "J" + row)?.ToString() ?? "0";
            string basis = XL.GetTextValue(sheet, "K" + row) ?? "Variable";
            string yield = XL.GetCellValue(sheet, "L" + row)?.ToString() ?? "1";

            structure.Add(new Component(childItem, qty, yield, basis));
        }

        private static void AddToItems(Excel.Worksheet sheet, int row,
           char firstColumn, FusionRibbon ribbon)
        {
            string number = XL.GetTextValue(sheet, ((char)firstColumn) + "" + row);

            if (String.IsNullOrWhiteSpace(number))
                throw new Exception(String.Format("Invalid item number in row {0}", row));

            if (ribbon.Items.Find(i => i.ItemNumber == number) == null)
            {
                string description = XL.GetTextValue(sheet, ((char)(firstColumn + 1)) + "" + row) ?? "";
                string uom = XL.GetTextValue(sheet, ((char)(firstColumn + 2)) + "" + row) ?? "";
                string matType = XL.GetTextValue(sheet, ((char)(firstColumn + 3)) + "" + row) ?? "";

                ribbon.Items.Add(new Item(number, ribbon.OrgCode, matType, ribbon.Config.DefaultItemClass)
                    .Set("ItemDescription", description)
                    .Set("PrimaryUOMValue", uom)
                    .Set("Template", ResolveTemplate(ribbon.Config, ribbon.OrgCode, ribbon.Product, matType)));

                ribbon.Items.Add(new Item(number, ribbon.Config.ItemMasterOrg, matType, ribbon.Config.DefaultItemClass)
                    .Set("ItemDescription", description)
                    .Set("PrimaryUOMValue", uom)
                    .Set("Template", ResolveTemplate(ribbon.Config, ribbon.Config.ItemMasterOrg, ribbon.Product, matType)));
            }
        }

        private static string ResolveTemplate(Config config, string orgCode, string product, string matType)
        {
            return config.templates.Find(t => t.OrgCode == orgCode && t.Product == product && t.MaterialType == matType)?.ItemTemplate ??
                config.DefaultItemTemplate;
        }
    }
}
