﻿namespace Insight.Fusion.BOMAddIn
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class WorkDefinitionRequest
    {
        [JsonProperty("workDefinitionHeaders", NullValueHandling = NullValueHandling.Ignore)]
        public List<WorkDefinitionHeader> WorkDefinitionHeaders { get; set; }
    }

    public partial class WorkDefinitionHeader
    {
        [JsonProperty("ActionCode", NullValueHandling = NullValueHandling.Ignore)]
        public string ActionCode { get; set; }

        [JsonProperty("ItemNumber", NullValueHandling = NullValueHandling.Ignore)]
        public string ItemNumber { get; set; }

        [JsonProperty("OrganizationCode", NullValueHandling = NullValueHandling.Ignore)]
        public string OrganizationCode { get; set; }

        [JsonProperty("VersionNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? VersionNumber { get; set; }

        [JsonProperty("WorkMethodCode", NullValueHandling = NullValueHandling.Ignore)]
        public string WorkMethodCode { get; set; }

        [JsonProperty("WorkDefinitionCode", NullValueHandling = NullValueHandling.Ignore)]
        public string WorkDefinitionCode { get; set; }

        [JsonProperty("ItemStructureName", NullValueHandling = NullValueHandling.Ignore)]
        public string ItemStructureName { get; set; }

        [JsonProperty("CompletionSubinventoryCode", NullValueHandling = NullValueHandling.Ignore)]
        public string CompletionSubinventoryCode { get; set; }

        [JsonProperty("ProductionPriority", NullValueHandling = NullValueHandling.Ignore)]
        public long? ProductionPriority { get; set; }

        [JsonProperty("CostingPriority", NullValueHandling = NullValueHandling.Ignore)]
        public long? CostingPriority { get; set; }

        [JsonProperty("operations", NullValueHandling = NullValueHandling.Ignore)]
        public List<Operation> Operations { get; set; }
    }

    public partial class Operation
    {
        [JsonProperty("ActionCode", NullValueHandling = NullValueHandling.Ignore)]
        public string ActionCode { get; set; }

        [JsonProperty("OperationSequenceNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? OperationSequenceNumber { get; set; }

        [JsonProperty("StandardOperationCode", NullValueHandling = NullValueHandling.Ignore)]
        public string StandardOperationCode { get; set; }

        [JsonProperty("OperationType", NullValueHandling = NullValueHandling.Ignore)]
        public string OperationType { get; set; }
    }

    public partial class WorkDefinitionRequest
    {
        public static WorkDefinitionRequest FromJson(string json) => JsonConvert.DeserializeObject<WorkDefinitionRequest>(json, Insight.Fusion.BOMAddIn.Converter.Settings);
    }

    public partial class WorkDefinitionResponse
    {
        [JsonProperty("SourceSystemCode", NullValueHandling = NullValueHandling.Ignore)]
        public string SourceSystemCode { get; set; }

        [JsonProperty("SourceSystemType", NullValueHandling = NullValueHandling.Ignore)]
        public string SourceSystemType { get; set; }

        [JsonProperty("ErrorsExistFlag", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(PurpleParseStringConverter))]
        public bool? ErrorsExistFlag { get; set; }

        [JsonProperty("workDefinitionHeaders", NullValueHandling = NullValueHandling.Ignore)]
        public List<WorkDefinitionHeader> WorkDefinitionHeaders { get; set; }

        [JsonProperty("links", NullValueHandling = NullValueHandling.Ignore)]
        public List<Link> Links { get; set; }
    }

    public partial class Link
    {
        [JsonProperty("rel", NullValueHandling = NullValueHandling.Ignore)]
        public string Rel { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("kind", NullValueHandling = NullValueHandling.Ignore)]
        public Kind? Kind { get; set; }

        [JsonProperty("properties", NullValueHandling = NullValueHandling.Ignore)]
        public LinkProperties Properties { get; set; }
    }

    public partial class LinkProperties
    {
        [JsonProperty("changeIndicator", NullValueHandling = NullValueHandling.Ignore)]
        public string ChangeIndicator { get; set; }
    }

    public partial class WorkDefinitionHeader
    {[JsonProperty("ErrorMessages", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorMessages { get; set; }

        [JsonProperty("ErrorMessageNames", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorMessageNames { get; set; }

        [JsonProperty("CostingBatchOutputSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? CostingBatchOutputSize { get; set; }

        [JsonProperty("SerialTrackingFlag", NullValueHandling = NullValueHandling.Ignore)]
        public bool? SerialTrackingFlag { get; set; }

        [JsonProperty("TransformFromItemNumber", NullValueHandling = NullValueHandling.Ignore)]
        public string TransformFromItemNumber { get; set; }

        [JsonProperty("CompletionLocator", NullValueHandling = NullValueHandling.Ignore)]
        public string CompletionLocator { get; set; }

        [JsonProperty("WorkDefinitionId", NullValueHandling = NullValueHandling.Ignore)]
        public long? WorkDefinitionId { get; set; }

        [JsonProperty("BatchQuantity", NullValueHandling = NullValueHandling.Ignore)]
        public long? BatchQuantity { get; set; }

        [JsonProperty("BatchUOMCode", NullValueHandling = NullValueHandling.Ignore)]
        public string BatchUomCode { get; set; }

        [JsonProperty("ProcessName", NullValueHandling = NullValueHandling.Ignore)]
        public string ProcessName { get; set; }

        [JsonProperty("StatusCode", NullValueHandling = NullValueHandling.Ignore)]
        public string StatusCode { get; set; }

        [JsonProperty("MaintenanceCode", NullValueHandling = NullValueHandling.Ignore)]
        public string MaintenanceCode { get; set; }

        [JsonProperty("MaintenanceName", NullValueHandling = NullValueHandling.Ignore)]
        public string MaintenanceName { get; set; }

        [JsonProperty("MaintenanceDescription", NullValueHandling = NullValueHandling.Ignore)]
        public string MaintenanceDescription { get; set; }

        [JsonProperty("links", NullValueHandling = NullValueHandling.Ignore)]
        public List<Link> Links { get; set; }
    }

    public partial class Operation
    {

        [JsonProperty("ErrorMessages", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorMessages { get; set; }

        [JsonProperty("ErrorMessageNames", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorMessageNames { get; set; }

        [JsonProperty("ReferencedFlag", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ReferencedFlag { get; set; }

        [JsonProperty("OperationName", NullValueHandling = NullValueHandling.Ignore)]
        public string OperationName { get; set; }

        [JsonProperty("OperationDescription", NullValueHandling = NullValueHandling.Ignore)]
        public string OperationDescription { get; set; }

        [JsonProperty("WorkCenterCode", NullValueHandling = NullValueHandling.Ignore)]
        public string WorkCenterCode { get; set; }

        [JsonProperty("CountPointOperationFlag", NullValueHandling = NullValueHandling.Ignore)]
        public bool? CountPointOperationFlag { get; set; }

        [JsonProperty("AutoTransactFlag", NullValueHandling = NullValueHandling.Ignore)]
        public bool? AutoTransactFlag { get; set; }

        [JsonProperty("SerialTrackingFlag", NullValueHandling = NullValueHandling.Ignore)]
        public bool? SerialTrackingFlag { get; set; }

        [JsonProperty("OSPItemNumber", NullValueHandling = NullValueHandling.Ignore)]
        public string OspItemNumber { get; set; }

        [JsonProperty("SupplierName", NullValueHandling = NullValueHandling.Ignore)]
        public string SupplierName { get; set; }

        [JsonProperty("SupplierSiteCode", NullValueHandling = NullValueHandling.Ignore)]
        public string SupplierSiteCode { get; set; }

        [JsonProperty("ShippingDocumentsFlag", NullValueHandling = NullValueHandling.Ignore)]
        public string ShippingDocumentsFlag { get; set; }

        [JsonProperty("LeadTimeUnitOfMeasure", NullValueHandling = NullValueHandling.Ignore)]
        public string LeadTimeUnitOfMeasure { get; set; }

        [JsonProperty("FixedLeadTime", NullValueHandling = NullValueHandling.Ignore)]
        public long? FixedLeadTime { get; set; }

        [JsonProperty("VariableLeadTime", NullValueHandling = NullValueHandling.Ignore)]
        public long? VariableLeadTime { get; set; }

        [JsonProperty("OptionDependentFlag", NullValueHandling = NullValueHandling.Ignore)]
        public bool? OptionDependentFlag { get; set; }

        [JsonProperty("PlanningPercent", NullValueHandling = NullValueHandling.Ignore)]
        public double? PlanningPercent { get; set; }

        [JsonProperty("LeadTimePercent", NullValueHandling = NullValueHandling.Ignore)]
        public double? LeadTimePercent { get; set; }

        [JsonProperty("AddlMtlAtManualIssue", NullValueHandling = NullValueHandling.Ignore)]
        public string AddlMtlAtManualIssue { get; set; }

        [JsonProperty("OperationComplWithUnderIssue", NullValueHandling = NullValueHandling.Ignore)]
        public string OperationComplWithUnderIssue { get; set; }

        [JsonProperty("OperationComplWithOpenExceptions", NullValueHandling = NullValueHandling.Ignore)]
        public string OperationComplWithOpenExceptions { get; set; }

        [JsonProperty("links", NullValueHandling = NullValueHandling.Ignore)]
        public List<Link> Links { get; set; }
    }

    public enum Kind { Collection, Item };

    public partial class WorkDefinitionResponse
    {
        public static WorkDefinitionResponse FromJson(string json) => JsonConvert.DeserializeObject<WorkDefinitionResponse>(json, Insight.Fusion.BOMAddIn.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this WorkDefinitionResponse self) => JsonConvert.SerializeObject(self, Insight.Fusion.BOMAddIn.Converter.Settings);
        public static string ToJson(this WorkDefinitionRequest self) => JsonConvert.SerializeObject(self, Insight.Fusion.BOMAddIn.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                KindConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class PurpleParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(bool) || t == typeof(bool?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            
            if (Boolean.TryParse(value, out bool b))
            {
                return b;
            }
            throw new Exception("Cannot unmarshal type bool");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (bool)untypedValue;
            var boolString = value ? "true" : "false";
            serializer.Serialize(writer, boolString);
            return;
        }        

        public static readonly PurpleParseStringConverter Singleton = new PurpleParseStringConverter();
    }

    internal class KindConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Kind) || t == typeof(Kind?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "collection":
                    return Kind.Collection;
                case "item":
                    return Kind.Item;
            }
            throw new Exception("Cannot unmarshal type Kind");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Kind)untypedValue;
            switch (value)
            {
                case Kind.Collection:
                    serializer.Serialize(writer, "collection");
                    return;
                case Kind.Item:
                    serializer.Serialize(writer, "item");
                    return;
            }
            throw new Exception("Cannot marshal type Kind");
        }

        public static readonly KindConverter Singleton = new KindConverter();
    }

    internal class FluffyParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);

            if (Int64.TryParse(value, out long l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly FluffyParseStringConverter Singleton = new FluffyParseStringConverter();
    }
}
