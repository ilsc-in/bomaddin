using Insight.Fusion.Utils;
using System;
using System.Collections.Generic;

namespace Insight.Fusion.BOMAddIn
{
    public class Item : FusionObject
    {
        public bool Exists { get; set; }
        public bool IsFG { get; set; }
        public bool WDExists { get; set; }
        public string ItemNumber { get { return Attributes.GetValueOrDefault("ItemNumber", "INVALID_ITEM"); } }
        public string OrgCode { get { return Attributes.GetValueOrDefault("OrganizationCode", "INVALID_ORG"); } }
        public string ItemClass { get { return Attributes.GetValueOrDefault("ItemClass", "Root Item Class"); } }


        public Item(string number, string orgCode, string matType, string itemClass = "Root Item Class")
        {
            if (String.IsNullOrWhiteSpace(number) || String.IsNullOrWhiteSpace(orgCode))
                throw new ArgumentException("Item Number and Organization Code can't be null or blank");

            IsFG = matType.Equals("FERT") || matType.Equals("HALB");

            Attributes = new Dictionary<string, string>(){
                {"ItemNumber", number},
                {"OrganizationCode", orgCode},
                {"ItemClass", itemClass}
            };
        }

        public new Item Set(String attr, String value)
        {
            base.Set(attr, value);
            return this;
        }

        public override string ToString()
        {
            return String.Format("ItemNumber: {0}, OrgCode: {1}, ItemClass: {2}, Exists: {3}", ItemNumber, OrgCode,
            ItemClass, Exists);
        }
    }
}